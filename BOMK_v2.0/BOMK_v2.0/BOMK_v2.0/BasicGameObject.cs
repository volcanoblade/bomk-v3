﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace BOMK_v2._0
{
    public class BasicGameObject : IClickable, IDrawablenBetter
    {
        protected string thisTexture;
        protected string type;
        protected MouseState previousMouseState;
        protected bool isFocused;
        private Vector2 middleVector;
        public Player connectedPlayer;
        protected int buildCost;
        /// <summary>
        /// 
        /// </summary>
        private Vector2 circleRadius;
        /// <summary>
        /// 
        /// </summary>
        protected Game1 game;
        /// <summary>
        /// Position on the left-upper corner as a vector from 0,0 of the MAP. no connection with the camara.
        /// </summary>
        public Vector2 realPos;

        /// <summary>
        /// Position on the left-upper corner as a vector from 0,0. Camara uses this.
        /// </summary>
        protected Vector2 drawPos;
        /// <summary>
        /// Texture of the unit that is displayed.
        /// </summary>
        private Texture2D texture;
        //drawPos get&set
        public bool GetIsFocused()
        {
            return this.isFocused;
        }
        public void SetIsFocused(bool isFocused)
        {
            this.isFocused = isFocused;
        }
        public Vector2 GetDrawPos()
        {
            return this.drawPos;
        }
        public string GetTexture()
        {
            foreach (BasicGameObject o in this.game.GetObjectList())
            {
                if (o is Base && o != null && o.isFocused)
                {
                    if (o.connectedPlayer.isHumanPlayer)
                        return "BlueBase";
                    else
                        return "RedBase";
                }
                else if (o is Barracks && o != null && o.isFocused)
                {
                    if (o.connectedPlayer.isHumanPlayer)
                        return "BlueBarracks";
                    else
                        return "RedBarracks";
                }
                else if (o is Tower && o != null && o.isFocused)
                {
                    if (o.connectedPlayer.isHumanPlayer)
                        return "BlueTower";
                    else
                        return "RedTower";
                }
                else if (o is House && o != null && o.isFocused)
                {
                    if (o.connectedPlayer.isHumanPlayer)
                        return "BlueHouse";
                    else
                        return "RedHouse";
                }
                else if (o is RangedUnit && o != null && o.isFocused)
                {
                    if (o.connectedPlayer.isHumanPlayer)
                        return "BlueCombatUnit";
                    else
                        return "RedCombatUnit";
                }
                else if (o is MeleeUnit && o != null && o.isFocused)
                {
                    if (o.connectedPlayer.isHumanPlayer)
                        return "BlueMeleeUnit";
                    else
                        return "RedMeleeUnit";
                }
                else if (o is WorkUnit && o != null && o.isFocused)
                {
                    if (o.connectedPlayer.isHumanPlayer)
                        return "BlueWorkUnit";
                    else
                        return "RedWorkUnit";
                }
            }
            return "";
        }
        public string GetObjectType()
        {
            foreach (BasicGameObject o in this.game.GetObjectList())
            {
                if (o is Resource && o !=null && o.isFocused)
                {
                    Resource r = o as Resource;
                    return "Resource";
                }
                else if (o is Base&&o!=null && o.isFocused)
                {
                    Base b = o as Base;
                    return "Base";
                    
                }
                else if (o is Barracks && o != null && o.isFocused)
                {
                    Barracks br = o as Barracks;
                    return "Barracks";
                }
                else if (o is Tower && o != null && o.isFocused)
                {
                    Tower t = o as Tower;
                    return "Tower";
                }
                else if (o is House && o != null && o.isFocused)
                {
                    House h = o as House;
                    return "House";
                }
                else if (o is RangedUnit && o != null && o.isFocused)
                {
                    RangedUnit ru = o as RangedUnit;
                    return "RangedUnit";
                }
                else if (o is MeleeUnit && o != null && o.isFocused)
                {
                    MeleeUnit mu = o as MeleeUnit;
                    return "MeleeUnit";
                }
                else if (o is WorkUnit && o != null && o.isFocused)
                {
                    WorkUnit wu = o as WorkUnit;
                    return "WorkUnit";
                }
               
            }
            return "";
        }

        /// <summary>
        /// Constructor - takes drawpos, texture and game.
        /// </summary>
        /// <param name="realPos"></param>
        /// <param name="texture"></param>
        /// <param name="game"></param>
        public BasicGameObject(Vector2 realPos, Texture2D texture, Game1 game, Player connectedPlayer)
        {
            this.game = game;
            this.texture = texture;
            this.realPos = realPos;
            this.drawPos = realPos - this.game.cameraPos;
            this.connectedPlayer = connectedPlayer;
        }
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, this.drawPos, Color.White);
        }
        public Rectangle GetDisplayRectangle()
        {
            Rectangle Area = new Rectangle();
            Area.X = (int)drawPos.X;
            Area.Width = texture.Width;
            Area.Y = (int)drawPos.Y;
            Area.Height = texture.Height;
            return Area;
        }
        public Vector2 GetCircleRadius()
        {
            return this.circleRadius;
        }
        public void SetCircleRadius(Vector2 circleRadius)
        {
            this.circleRadius = circleRadius;
        }
        public Vector2 GetMiddleVector()
        {
            return this.middleVector;
        }
        public void SetMiddleVector(Vector2 middleVector)
        {
            this.middleVector = middleVector;
        }
        public void calcRadius()
        {
            this.middleVector.X = this.realPos.X + texture.Width / 2;
            this.middleVector.Y = this.realPos.Y + texture.Height / 2;
            this.circleRadius = - realPos + middleVector;
        }

        public virtual void DetectClick()
        {
            if (this.connectedPlayer != null || this is Resource)
            {
                MouseState state = Mouse.GetState();
                if (state.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released)
                {
                    Rectangle area = GetDisplayRectangle();
                    Point mousePosition = new Point(state.X, state.Y);
                    if (area.Contains(mousePosition))
                    {
                        this.isFocused = true;
                        OnClick(mousePosition);
                    }
                    else
                    {
                        this.isFocused = false;
                    }
                }
                previousMouseState = state;
            }
        }
        protected virtual void OnClick(Point mousePosition)
        {
                game.SetFocusedObject(this);
        }

        /// <summary>
        /// Loads all functions and needed calculations for every game time.
        /// </summary>
        public virtual void loadObjectProperties()
        {
            this.calcRadius();
            this.drawPos = this.realPos - this.game.cameraPos;
        }
        /// <summary>
        /// Called by the game every update.
        /// Handles everything this object needs to do or call, no matter what class he belongs to.
        /// </summary>
        public void objectBehaviour()
        {
            if (this is Unit)
            {
                Unit u = this as Unit;
                u.UnitBehaviour();
            }
            else if (this is ImaginaryBuilding)
            {
                ImaginaryBuilding ib = this as ImaginaryBuilding;
                ib.MouseFollow();
            }
            else if (this is Tower)
            {
                Tower t = this as Tower;
                t.TowerAttack();
            }
            else if (this is Projectile)
            {
                Projectile p = this as Projectile;
                p.Proceed();
            }
            else if (this is RepairProjectile)
            {
                RepairProjectile p = this as RepairProjectile;
                p.Proceed();
            }

            else if (this is BasicButton)
            {
                BasicButton b = this as BasicButton;
                b.ButtonCheck();
                b.ButtonFollow();
            }
            else if (this is Resource)
            {
                Resource r = this as Resource;
                r.Dying();
            }
        }
    }
}
