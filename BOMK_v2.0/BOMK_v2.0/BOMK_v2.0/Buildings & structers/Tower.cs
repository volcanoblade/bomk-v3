﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    class Tower : Building
    {
        private float timeSinceLastShot = 2f;
        private float attackSpeed;
        private float range;
        private bool hasTarget;

        public Tower(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game, connectedPlayer)
        {
            this.damage = 5;
            this.type = "Tower";
            this.buildCost = this.game.towerCost;
            this.attackSpeed = 0.01f;
            this.range = 180;
            this.SetMaxHp(300);
            this.SetHp(300);
            this.connectedPlayer.playerResource = this.connectedPlayer.playerResource - this.buildCost;
        }
        public int GetDamage()
        {
            return this.damage;
        }
        public void TowerAttack()
        {
            BasicGameObjectWithHp target;
            foreach (BasicGameObject o in this.game.GetObjectList().ToList())
            {
                if (o is BasicGameObjectWithHp && o.connectedPlayer != this.connectedPlayer)
                {
                    target = o as BasicGameObjectWithHp;
                    Vector2 distance = target.GetMiddleVector() - this.GetMiddleVector();
                    timeSinceLastShot += this.attackSpeed;
                    if (this != target && target != null && target.GetHp() > 0)
                    {
                        if (this.GetCircleRadius().Length() + target.GetCircleRadius().Length() + this.range >= distance.Length() && !hasTarget)
                        {
                            hasTarget = true;
                            if (timeSinceLastShot >= 2f)
                            {
                                Projectile shot = new Projectile(this.game.Content.Load<Texture2D>("Shot"), this.GetMiddleVector(), this.game, target, this.connectedPlayer,this );
                                this.game.AddToObjectList(shot);
                                timeSinceLastShot = 0f;
                            }
                        }
                        else
                        {
                            hasTarget = false;
                        }
                    }
                }
            }
        }
        protected override void OnClick(Point mousePosition)
        {
            if (!this.hasMenu)
            {
                Vector2 menuVector = new Vector2(0, 382);
                TowerMenu menu = new TowerMenu(menuVector, this.game.Content.Load<Texture2D>("MenuEmptyTexture"), this.game, this, this.connectedPlayer);
                menu.CreateMenu();
                this.connectedPlayer.AddObjectToPlayer(this);
                this.hasMenu = true;
            }
        }
    }
}
