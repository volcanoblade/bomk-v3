﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace BOMK_v2._0
{
    class House : Building
    {
        public House(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game, connectedPlayer)
        {
            this.type = "Farm";
            this.buildCost = this.game.houseCost;
            this.SetMaxHp(100);
            this.SetHp(100);
            this.connectedPlayer.buildingCapacity = this.connectedPlayer.buildingCapacity + 5;
            this.connectedPlayer.playerResource = this.connectedPlayer.playerResource - this.buildCost;
            if (!this.connectedPlayer.isHumanPlayer)
            {
                this.connectedPlayer.computerAI.shouldBuildHouse = false;
                this.connectedPlayer.computerAI.builtHouse = true;
                this.connectedPlayer.computerAI.houseOrderGiven = false;
            }
        }
        protected override void OnClick(Point mousePosition)
        {
            Vector2 menuVector = new Vector2(0, 382);
            FarmMenu menu = new FarmMenu(menuVector, this.game.Content.Load<Texture2D>("MenuEmptyTexture"), this.game, this, connectedPlayer);
            menu.CreateMenu();
        }
    }
}