﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace BOMK_v2._0
{
    public class Base : Building
    {
        int timeToCreate;
        public SoundEffect workerCreate;
        public Base(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game, connectedPlayer)
        {
            this.type = "Base";
            this.buildCost = this.game.baseCost;
            this.connectedPlayer.playerResource = this.connectedPlayer.playerResource - this.buildCost;
            this.workerCreate = this.game.Content.Load<SoundEffect>("buildercreate");
        }
        protected override void OnClick(Point mousePosition)
        {
            if (!this.hasMenu)
            {
                Vector2 menuVector = new Vector2(0, 382);
                BaseMenu menu = new BaseMenu(menuVector, this.game.Content.Load<Texture2D>("MenuEmptyTexture"), this.game, this, this.connectedPlayer);
                menu.createMenu();
                this.connectedPlayer.AddObjectToPlayer(this);
                this.hasMenu = true;
            }
        }
        public void CreateWorker()
        {
            if ((this.connectedPlayer.buildingUsage == this.connectedPlayer.buildingCapacity) && this.connectedPlayer.isHumanPlayer)
                return;
            bool finished = false;
            if (this.timeToCreate <= 0)
            {
                Texture2D createdTexture;
                Vector2 createdVector = new Vector2(this.GetMiddleVector().X, this.GetMiddleVector().Y);
                if (this.connectedPlayer.isHumanPlayer)
                {
                    createdTexture = game.Content.Load<Texture2D>("BlueWorkUnit");
                }
                else
                {
                    createdTexture = game.Content.Load<Texture2D>("RedWorkUnit");
                }
                WorkUnit createdUnit = new WorkUnit(createdVector, createdTexture, this.game, this.connectedPlayer);
                if (createdUnit.connectedPlayer.playerResource >= 5)
                {
                    this.connectedPlayer.AddObjectToPlayer(createdUnit);
                    if (this.connectedPlayer.isHumanPlayer)
                        workerCreate.Play();
                }
                finished = true;
            }
            else if (finished == true)
            {
                timeToCreate = 100;
            }
            else
            {
                timeToCreate--;
            }

        }
    }
}
