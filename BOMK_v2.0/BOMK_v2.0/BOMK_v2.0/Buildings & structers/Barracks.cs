﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    public class Barracks : Building
    {
        public SoundEffect meleeCreate;
        public SoundEffect rangedCreate;
        public Barracks(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game,connectedPlayer)
        {
            this.type = "Barracks";
            this.buildCost = this.game.barracksCost;
            this.connectedPlayer.playerResource = this.connectedPlayer.playerResource - this.buildCost;
            this.meleeCreate = this.game.Content.Load<SoundEffect>("meleecreate");
            this.rangedCreate = this.game.Content.Load<SoundEffect>("rangecreate");
        }
        protected override void OnClick(Point mousePosition)
        {
            if(!this.hasMenu)
            {
                Vector2 menuVector = new Vector2(0, 382);
                BarracksMenu menu = new BarracksMenu(menuVector, this.game.Content.Load<Texture2D>("MenuEmptyTexture"), this.game, this,this.connectedPlayer);
                menu.createMenu();
                game.AddToObjectList(menu);
                this.hasMenu = true;
            }
        }

        /// <summary>
        /// For the AI to reference instead of pressing a button.
        /// I use this trhough the barracks and not a button or something else because its more logical and also units cant be created without
        /// an ordering barracks.
        /// </summary>
        /// <param name="type"></param>
        public void BuildUnits(string type)
        {
            if ((this.connectedPlayer.buildingCapacity == this.connectedPlayer.buildingUsage)&& this.connectedPlayer.isHumanPlayer)
                return;
                if (!this.connectedPlayer.isHumanPlayer)
                {
                    if (type == "Melee" && this.connectedPlayer.playerResource > this.game.meleeCost)
                    {
                        float x = this.GetMiddleVector().X;
                        MeleeUnit u = new MeleeUnit(new Vector2(this.realPos.X+10, this.realPos.Y-10), this.game.Content.Load<Texture2D>("RedMeleeUnit"), this.game, this.connectedPlayer);
                        this.connectedPlayer.AddObjectToPlayer(u);
                    }
                    else if (type == "Ranged" && this.connectedPlayer.playerResource > this.game.rangedCost)
                    {
                        RangedUnit u = new RangedUnit(new Vector2(this.realPos.X + 10, this.realPos.Y - 10), this.game.Content.Load<Texture2D>("RedCombatUnit"), this.game, this.connectedPlayer);
                        this.connectedPlayer.AddObjectToPlayer(u);
                    }
                }
                else  // no need to check anything for the human player. Resource and building capactiy are checked in the button before this is called.
                {
                    if (type == "Melee")
                    {
                        MeleeUnit u = new MeleeUnit(new Vector2(this.GetMiddleVector().X, this.GetMiddleVector().Y), this.game.Content.Load<Texture2D>("BlueMeleeUnit"), this.game, this.connectedPlayer);
                        this.connectedPlayer.AddObjectToPlayer(u);
                        meleeCreate.Play();
                    }
                    else if (type == "Ranged")
                    {
                        RangedUnit u = new RangedUnit(new Vector2(this.GetMiddleVector().X, this.GetMiddleVector().Y), this.game.Content.Load<Texture2D>("BlueCombatUnit"), this.game, this.connectedPlayer);
                        this.connectedPlayer.AddObjectToPlayer(u);
                        rangedCreate.Play();
                    }
                }           
        }
    }
}
