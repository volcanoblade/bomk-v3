﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace BOMK_v2._0
{
    public class ImaginaryBuilding : Building
    {

        public ImaginaryBuilding(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game, connectedPlayer)
        {

        }
        public void MouseFollow()
        {
            Vector2 followMouse = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            this.drawPos = followMouse;
        }
        public void Delete()
        {
            this.connectedPlayer.RemoveObjectFromPlayer(this);
        }

    }
}
