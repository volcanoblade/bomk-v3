﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace BOMK_v2._0
{
    public class Resource : BasicGameObject
    {
        public int supply;
        public Resource(Vector2 drawPos, Texture2D texture, Game1 game,Player connectedPlayer) :base(drawPos,texture,game,connectedPlayer)
        {
            this.type = "Resource";
            this.supply = 1000;
        }

        /// <summary>
        ///  Takes a WorkUnit that wants to take resources.
        /// Decreases amount of resources in the resource and adds some to the players sum.
        /// </summary>
        /// <param name="unit"></param>
        public void ResourceTransaction(WorkUnit unit)
        {
            if (supply>0)
            {  
            this.supply--;
            unit.connectedPlayer.addSupply();
            }
        }

        public void Dying()
        {
            if (supply <= 0)
            {
                this.game.RemoveFromObjectList(this);
                this.isFocused = false;
            }
        }

        protected override void OnClick(Point mousePosition)
        {
            this.game.SetFocusedObject(this);
            this.isFocused = true;
        }
    }
}
