﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace BOMK_v2._0
{
    public class Building : BasicGameObjectWithHp
    {
        public Building(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game, connectedPlayer)
        {
            this.SetMaxHp(1000);
            this.SetHp(1000);
        }
        public override void DetectClick()
        {
            if (this.connectedPlayer.isHumanPlayer)
            {
                MouseState state = Mouse.GetState();
                if (state.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released)
                {

                    Rectangle area = GetDisplayRectangle();
                    Point mousePosition = new Point(state.X, state.Y);
                    if (area.Contains(mousePosition))
                    {
                        this.isFocused = true;
                        OnClick(mousePosition);

                    }
                    else
                    {
                        this.isFocused = false;
                    }

                }
                previousMouseState = state;
            }
        }

    }
}
