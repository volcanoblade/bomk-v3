﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOMK_v2._0
{
    public class Squad
    {
        public List<Unit> squadMembers;
        public AIManager ai;

        /// <summary>
        /// Constructor.
        /// Takes - nothing.
        /// Creates an instance of the squadMember's list.
        /// </summary>
        public Squad(AIManager ai)
        {
            this.ai = ai;
            this.squadMembers = new List<Unit>();
            this.ai.aiSquadList.Add(this); // squad adds itself to the ai list of squads.
        }

        public void AddMember(Unit unit)
        {
            this.squadMembers.Add(unit);
        }

        public void RemoveMember(Unit unit)
        {
            this.squadMembers.Remove(unit);
        }
        public void OrderAttack(BasicGameObjectWithHp target)
        {
            foreach (Unit unit in this.squadMembers.ToList())
            {
                if (unit is RangedUnit)
                {
                    RangedUnit rangedUnit = unit as RangedUnit;
                    this.ai.OrderRangedUnitToAttack(rangedUnit, target);
                }
                else if (unit is MeleeUnit)
                {
                    MeleeUnit meleeUnit = unit as MeleeUnit;
                    this.ai.OrderMeleeUnitToAttack(meleeUnit, target);
                }
            }
        }

        public void DisassembleSquad()
        {

        }
    }
}
