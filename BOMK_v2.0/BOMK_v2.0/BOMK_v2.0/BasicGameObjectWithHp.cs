﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    public class BasicGameObjectWithHp : BasicGameObject
    {
        protected int damage;
        private int hp;
        private int maxHp;
        public bool hasMenu;
        public bool targeted;
        protected SoundEffect workerDead;
        protected SoundEffect meleeDead;
        protected SoundEffect rangedDead;
        public BasicGameObjectWithHp(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game, connectedPlayer)
        {
            this.workerDead = this.game.Content.Load<SoundEffect>("builderdead");
            this.meleeDead = this.game.Content.Load<SoundEffect>("meleedead");
            this.rangedDead = this.game.Content.Load<SoundEffect>("rangedead");
        }
        public int GetDamage()
        {
            return this.damage;
        }
        public int GetMaxHp()
        {
            return this.maxHp;
        }
        public void SetMaxHp(int maxHp)
        {
            this.maxHp = maxHp;
        }
        public int GetHp()
        {
            return this.hp;
        }
        public void SetHp(int hp)
        {
            this.hp = hp;
        }
        /// <summary>
        /// If this unit's hp value is equal to 0 or less, it removes this unit from the game's object list
        /// by that, removing it from the game.
        /// </summary>
        public void dying()
        {
            if (this.hp > 0)
                return;
                this.connectedPlayer.RemoveObjectFromPlayer(this);
                if (this is Unit)   // if a unit dies the usage goes down.
                    this.connectedPlayer.buildingUsage--;
                else if (this is House)  // if a house dies the cap goes down.
                    this.connectedPlayer.buildingCapacity -= 5;

                if (this.connectedPlayer.isHumanPlayer)
                {
                    if (this is WorkUnit)
                        workerDead.Play();
                    if (this is MeleeUnit)
                        meleeDead.Play();
                    if (this is RangedUnit)
                        rangedDead.Play();
                }
            
        }

        /// <summary>
        /// Lets the unit die if needed.
        /// Calls the base load function.
        /// </summary>
        public override void loadObjectProperties()
        {
            this.dying();
            base.loadObjectProperties();
        }
    }
}




