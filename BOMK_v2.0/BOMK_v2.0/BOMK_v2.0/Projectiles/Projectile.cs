﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace BOMK_v2._0
{
    class Projectile : BasicGameObject
    {
        // so this little shot needs a daddy""
        // like a unit that sent it
        // and a recevier(?)
        // because the shot is sent to where to unit is now and not when it was at the moment of fire
        // so it needs to constantly check where is the attacked location
        //right?


        /// <summary>
        /// Constructor - Takes texture as Texture2D, a position to be drawen at as Vector2 and the game class as Game1
        /// </summary>

        private Vector2 difference;
        private Vector2 shotDestination;
        private BasicGameObjectWithHp target;
        private BasicGameObjectWithHp connectedObject;
        public Projectile(Texture2D texture, Vector2 drawPos, Game1 game, BasicGameObjectWithHp target,Player connectedPlayer, BasicGameObjectWithHp connectedUnit)
            : base(drawPos, texture, game,connectedPlayer)
        {
            this.connectedObject = connectedUnit;
            this.target = target;
        }

        public void Proceed() // not a good name
        {
            //function to update target()
            UpdateTarget();
            this.difference = -this.GetMiddleVector() + this.shotDestination;//look destination and 
            Vector2 newpos = difference;
            newpos.Normalize();
            if (this.GetDisplayRectangle().Intersects(target.GetDisplayRectangle())) // Means it got to its target
            {
                if (connectedObject is MeleeUnit)
                {
                    MeleeUnit unit = connectedObject as MeleeUnit;
                    this.target.SetHp(this.target.GetHp() - unit.GetDamage());
                    this.connectedPlayer.damageInflictied += unit.GetDamage();
                }
                else if (connectedObject is RangedUnit)
                {
                    RangedUnit unit = connectedObject as RangedUnit;
                  this.target.SetHp(this.target.GetHp() - unit.GetDamage());
                  this.connectedPlayer.damageInflictied += unit.GetDamage();
                }
                else if (connectedObject is Tower)
                {
                    Tower tower = connectedObject as Tower;
                    this.target.SetHp(this.target.GetHp() - tower.GetDamage());
                    this.connectedPlayer.damageInflictied += tower.GetDamage();
                }
               
                this.game.RemoveFromObjectList(this);
            }
            else if (this.target.GetHp() <= 0)
            {
                this.game.RemoveFromObjectList(this);
            }
            else 
            {
                realPos += newpos*2;
            }
        }
        public void UpdateTarget()
        {
            this.shotDestination = this.target.GetMiddleVector();
        }
        public override void DetectClick()
        {
            //nothing.
        }
    }
}
