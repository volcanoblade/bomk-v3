﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    class BasicMenu : BasicGameObject
    {
        protected BasicGameObject linkedObject;
        public BasicMenu(Vector2 drawPos, Texture2D texture, Game1 game,BasicGameObject linkedObject)
            : base(drawPos, texture, game)
        {
            this.linkedObject = linkedObject;
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!(game.GetFocusedObject() == this.linkedObject))
            {
                return;
            }
            else
            {
                base.Draw(spriteBatch);
            }
        }
    }
}
