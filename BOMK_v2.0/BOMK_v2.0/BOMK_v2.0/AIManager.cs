﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    public class AIManager
    {
        Random rnd = new Random();
        public Player computerPlayer;
        public Player humanPlayer;
        public Game1 game;
        public string currentState;
        public int timer = 0;
        public List<Squad> aiSquadList;
        public Point actionLocation;
        public double productionCounter;
        public double productionRate = 1;
        public bool shouldBuildHouse = false;
        public bool builtHouse = true;
        public bool houseOrderGiven = false;
        public int timeSinceTowerOrderGiven = 0;
        public int timeSinceBaseOrderGiven = 0;
        /// <summary>
        /// Constructor.
        /// Takes a both the human and computer class and the game class.
        /// </summary>
        /// <param name="computerPlayer"></param>
        /// <param name="humanPlayer"></param>
        /// <param name="game"></param>
        public AIManager(Player computerPlayer, Player humanPlayer, Game1 game)
        {
            this.productionCounter = 200;
            this.computerPlayer = computerPlayer;
            this.humanPlayer = humanPlayer;
            this.game = game;
            this.currentState = "Neutral";
            this.aiSquadList = new List<Squad>();
        }

        /// <summary>
        /// The main function that controls all of the computer's actions, states and productions.
        /// </summary>
        public void AIbehaviour()
        {
            ArmyDevelopment();
            this.currentState = StateAssesment();
            //--------------Resrouce Section-----------------------
            if (WorkersCollecting() < WorkersCollectingNeeded())
            {
                for (int i = 0; i < WorkersCollectingNeeded() - WorkersCollecting(); i++) // x collecting for every base. if not enough, then orders more.
                {
                    this.OrderUnitToCollect(this.GetAvailableWorker(), this.FindResourceNearByUnit(this.game.GetObjectList(),this.GetAvailableWorker()));   // makes 5 workers to collect at any time.
                }
            }
            //--------------Combat units production section---------------
            if (productionCounter > 0)
            {

                productionCounter--;
            }
            else if (productionCounter == 0 && this.HaveBarracks())
            {
                OrderBarracksToCreate(GetBarracks(), "Ranged");
                productionCounter = 250 * productionRate;
            }
            //--------------Base expansion----------------------------
            if (this.computerPlayer.playerResource >= 75 && this.timeSinceBaseOrderGiven >= 1000)
            {
                WorkUnit worker = this.GetAvailableWorker();
                this.OrderWorkUnitToBuild(worker, "Base",this.GetBaseBuildingVector(worker));
                this.timeSinceBaseOrderGiven = 0;
            }
            else
                timeSinceBaseOrderGiven++;
            //--------------Worker creation---------------
            if (AvailableWorkerCounter() < IdleWorkersNeeded())
            {
                //this.OrderEachBaseToCreateWorker();
                this.OrderBaseToCreateWorker(this.GetComputerBase()); // Keeps 2 idle workers at all times.
            }
            //--------------State Managment-------------------------
            if (currentState == "Start")
            {
                if (AvailableWorkerCounter() > 0)
                {
                    WorkUnit worker = this.GetAvailableWorker();
                    OrderWorkUnitToBuild(worker, "Barracks", new Vector2(GetComputerBase().GetMiddleVector().X + 200, GetComputerBase().GetMiddleVector().Y - 200));
                }
                if (this.HaveBarracks())
                {
                    OrderBarracksToCreate(GetBarracks(), "Melee");
                }
            }
            else if (currentState == "Aggresive")
            {
                this.OrderWorkUnitToRepairInjuredBuilding();
                this.OrderAllAvailableCombatUnitsToAttackNearestEnemy();
                //this.SendAllCombatUnitsTo(GetHumanBase()); maybe?
            }
            else if (currentState == "Defensive")
            {
                this.timeSinceTowerOrderGiven++;
                if (this.timeSinceTowerOrderGiven >= 500 && this.HaveAvailableWorker())
                    this.OrderAvailableWorkerToBuildTower();
                this.OrderWorkUnitToRepairInjuredBuilding();
                this.OrderUnitsToDefendBuildings();
            }
            else if (currentState == "Neutral")
            {
               this.OrderUnitsToAttackAndDefend(this.GetNumberOfCombatUnit()); // gets the number of combat units the player has and sends half to attack and half to defend.

            }
        }

        /// <summary>
        /// Takes a work unit and returns the vector which represents the ideal place for building a new tower.
        /// </summary>
        /// <returns></returns>
        public Vector2 GetTowerBuildingVector(WorkUnit workUnit)
        {
            int combatUnitsCounter = 0;
            int sumX = 0;
            int sumY = 0;
            float avgX = 0f;
            float avgY = 0f;
            Vector2 combatUnitMassVector;
            foreach (BasicGameObject obj in this.humanPlayer.playerObjects.ToList())
            {
                if (!(obj is CombatUnit))
                    continue;
                combatUnitsCounter++;
                sumX += (int)obj.GetMiddleVector().X;
                sumY += (int)obj.GetMiddleVector().Y;
            }
            avgX = sumX / combatUnitsCounter;
            avgY = sumY / combatUnitsCounter;
            combatUnitMassVector = new Vector2(avgX, avgY);   // this is the "avg" location of all the human's combat units.
            Vector2 towerBuildingVectorDirection = -workUnit.GetMiddleVector() + combatUnitMassVector;
            towerBuildingVectorDirection.Normalize();
            towerBuildingVectorDirection = towerBuildingVectorDirection * 200;
            return workUnit.GetMiddleVector() + towerBuildingVectorDirection;
        }

        /// <summary>
        /// Takes a work unit and returns the vector which represents the ideal place for building a new base.
        /// </summary>
        /// <param name="workUnit"></param>
        /// <returns></returns>
        public Vector2 GetBaseBuildingVector(WorkUnit workUnit)
        {
            Vector2 nearestResourceMassVector;
            Resource resource1;
            Resource resource2;
            Resource resource3;
            List<BasicGameObject> fakeGameObjectList = this.game.GetObjectList().ToList();
            float sumX = 0f;
            float sumY = 0f;
            float avgX = 0f;
            float avgY = 0f;
            resource1 = this.FindResourceNearByUnit(fakeGameObjectList,workUnit);
            fakeGameObjectList.Remove(resource1);
            resource2 = this.FindResourceNearByUnit(fakeGameObjectList, workUnit);
            fakeGameObjectList.Remove(resource2);
            resource3 = this.FindResourceNearByUnit(fakeGameObjectList, workUnit);
            if(resource1 == null || resource2 == null || resource3 == null)
                return(new Vector2(workUnit.GetMiddleVector().X + rnd.Next(-200,200),workUnit.GetMiddleVector().Y + rnd.Next(-200,200)));
            sumX = resource1.GetMiddleVector().X + resource2.GetMiddleVector().X + resource3.GetMiddleVector().X;
            sumY = resource1.GetMiddleVector().Y + resource2.GetMiddleVector().Y + resource3.GetMiddleVector().Y;
            avgX = sumX / 3;
            avgY = sumY / 3;
            nearestResourceMassVector = new Vector2(avgX, avgY);
            Vector2 BaseBuildingVectorDirection = -workUnit.GetMiddleVector() + nearestResourceMassVector;
            BaseBuildingVectorDirection.Normalize();
            BaseBuildingVectorDirection = BaseBuildingVectorDirection * 300;
            return workUnit.GetMiddleVector() + BaseBuildingVectorDirection;
        }

        /// <summary>
        /// Gets an available worker and orders it to build a tower at an ideal location.
        /// </summary>
        public void OrderAvailableWorkerToBuildTower()
        {
            WorkUnit worker = this.GetAvailableWorker();
            this.OrderWorkUnitToBuild(worker, "Tower", this.GetTowerBuildingVector(worker));
            this.timeSinceTowerOrderGiven = 0;
        }

        /// <summary>
        /// Orders all barrakcs to build at some rate that is dependent on the armies of both sides and in the amount of resources the computer has.
        /// </summary>
        /// 
        public void SetToDefensiveMode()
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is CombatUnit)
                {
                    CombatUnit u = o as CombatUnit;

                    u.SetToHoldGroundNearObject(this.GetComputerBase());

                }
            }
        }

        /// <summary>
        /// Casuses all of the computer's combat units to automatically attack an enemy that is near.
        /// </summary>
        public void AutomaticAttackAi()
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is CombatUnit)
                {
                    CombatUnit u = o as CombatUnit;
                    foreach (BasicGameObject o1 in this.humanPlayer.playerObjects.ToList())
                    {
                        if (o1 is BasicGameObjectWithHp)
                        {
                            BasicGameObjectWithHp objectWithHp = o1 as BasicGameObjectWithHp;
                            float distance = (-u.GetMiddleVector() + objectWithHp.GetMiddleVector()).Length();
                            if (distance < 200)
                            {
                                u.SetAttack(objectWithHp);
                            }
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Orders each base in the game to create a worker once.
        /// </summary>
        public void OrderEachBaseToCreateWorker()
        {
            foreach (BasicGameObject obj in this.computerPlayer.playerObjects.ToList())
            {
                if (!(obj is Base))
                    continue;
                Base b = obj as Base;
                this.OrderBaseToCreateWorker(b);
            }
        }

        /// <summary>
        /// Orders all combat units to defend objects. It prioratizes building that are hurt. 
        /// </summary>
        public void OrderUnitsToDefendBuildings()
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (!(o is Building))
                    continue;
                Building hpo = o as Building;
                foreach (BasicGameObject o1 in this.computerPlayer.playerObjects.ToList())
                {
                    if (!(o1 is CombatUnit))
                        continue;
                    CombatUnit combatUnit = o1 as CombatUnit;
                    //combatUnit.SetCombatToIdle(); //  So it wont accept 2 orders
                    if (hpo.GetHp() < hpo.GetMaxHp())
                        combatUnit.SetToHoldGroundNearObject(hpo);
                    else
                        combatUnit.SetToHoldGroundNearObject(this.GetComputerBase());

                }
            }
        }

        /// <summary>
        /// Orders units to repair building that are hurt. 
        /// </summary>
        public void OrderWorkUnitToRepairInjuredBuilding()
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (!(o is Building))
                    continue;
                Building b = o as Building;
                foreach (BasicGameObject o1 in this.computerPlayer.playerObjects.ToList())
                {
                    if (!(o1 is WorkUnit))
                        continue;
                    WorkUnit u = o1 as WorkUnit;
                    if (b.GetHp() < b.GetMaxHp() && !(u.Busy))
                        u.SetToRepair(b);
                }
            }
        }

        /// <summary>
        /// Takes an object and orders all combat units that are not busy to go and defend it.
        /// </summary>
        /// <param name="obj"></param>
        public void OrderAllAvaliableCombatUnitsToHoldGroundAtObject(BasicGameObject obj)
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is CombatUnit)
                {
                    CombatUnit combatUnit = o as CombatUnit;
                    if (!combatUnit.Busy)
                        combatUnit.SetToHoldGroundNearObject(obj);
                }
            }
        }
        /// <summary>
        /// returns the number of combat units the computer player has as int.
        /// </summary>
        /// <returns></returns>
        public int GetNumberOfCombatUnit()
        {
            int counter = 0;
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is CombatUnit)
                {
                    CombatUnit combatUnit = o as CombatUnit;
                    counter++;
                }
            }
            return counter;
        }

        /// <summary>
        /// Takes the number of combat units the computer has and sends half to defend and half to attack.
        /// </summary>
        /// <param name="numberOfCombatUnits"></param>
        public void OrderUnitsToAttackAndDefend(int numberOfCombatUnits)
        {
            if (numberOfCombatUnits <= 1)
                return;
            int numberOfUnitsToAttack = 0;
            //Sends half to attack
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (!(o is CombatUnit))
                    continue;
                CombatUnit combatUnit = o as CombatUnit;
                combatUnit.SetCombatToIdle();
                if (numberOfUnitsToAttack <= numberOfCombatUnits / 2)
                {
                    this.OrderCombatUnitToAttackNearestEnemy(combatUnit);
                    numberOfUnitsToAttack++;
                }

            }
            //Sends half to hold.
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is CombatUnit)
                {
                    CombatUnit combatUnit = o as CombatUnit;
                    //combatUnit.SetCombatToIdle();
                    if (!combatUnit.Busy)
                    {
                        combatUnit.SetToHoldGroundNearObject(this.GetComputerBase()); // Guards the first base in the list. 
                    }
                }
            }
        }

        /// <summary>
        /// Orders all combat units that are not busy to attack what ever enemy is near them.
        /// </summary>
        public void OrderAllAvailableCombatUnitsToAttackNearestEnemy()
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is CombatUnit)
                {
                    CombatUnit u = o as CombatUnit;
                    OrderCombatUnitToAttackNearestEnemy(u);
                }
            }
        }

        /// <summary>
        /// orders all combat units that are not busy to defend in place.
        /// </summary>
        public void OrderAllCombatUnitsToHoldGround()
        {
            foreach (BasicGameObject obj in this.computerPlayer.playerObjects.ToList())
            {
                if (obj is CombatUnit)
                {
                    CombatUnit combatUnit = obj as CombatUnit;
                    if (!combatUnit.Busy)
                    {
                        combatUnit.SetToHoldGround();
                    }
                }
            }
        }
        
        /// <summary>
        /// Orders all combat units that are not busy to defend the computer's base.
        /// </summary>
        public void OrderAllCombatUnitsToHoldBase()
        {
            foreach (BasicGameObject obj in this.computerPlayer.playerObjects.ToList())
            {
                if (obj is CombatUnit)
                {
                    CombatUnit combatUnit = obj as CombatUnit;
                    if (!combatUnit.Busy)
                    {
                        combatUnit.SetToHoldGroundNearObject(this.GetComputerBase());
                    }
                }
            }
        }
        
        /// <summary>
        /// takes a combat unit and orders it to attack its nearest enemy.
        /// </summary>
        /// <param name="u"></param>
        public void OrderCombatUnitToAttackNearestEnemy(CombatUnit u)
        {
            u.SetAttack(FindNearestEnemy(u));
        }

        /// <summary>
        /// Take a combat unit and returns its nearest object as a basic game object with hp.
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public BasicGameObjectWithHp FindNearestEnemy(CombatUnit unit)
        {
            if (unit != null)
            {
                float minimalVectorLength = float.PositiveInfinity;
                BasicGameObjectWithHp returnedObject = null;
                foreach (BasicGameObject o in this.humanPlayer.playerObjects.ToList())
                {
                    if (o is BasicGameObjectWithHp)
                    {
                        BasicGameObjectWithHp oWithHp = o as BasicGameObjectWithHp;
                        float distance = (-unit.GetMiddleVector() + oWithHp.GetMiddleVector()).Length();
                        if (minimalVectorLength > distance)
                        {
                            returnedObject = oWithHp;
                            minimalVectorLength = distance;
                        }
                    }
                }
                return returnedObject;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Returns the number of idle workers the computer player needs.
        /// </summary>
        /// <returns></returns>
        public int IdleWorkersNeeded()
        {
            int baseNumber = 0;
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is Base)
                {
                    baseNumber++;
                }
            }
            return baseNumber * 2;
        }
        
        /// <summary>
        /// Returns the number of collecting workers the computer player needs.
        /// </summary>
        /// <returns></returns>
        public int WorkersCollectingNeeded()
        {
            int baseNumber = 0;
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (!(o is Base))
                    continue;
                    Base b = o as Base;
                    if (!b.connectedPlayer.isHumanPlayer)
                    {
                        baseNumber++;
                    }           
            }
            return (baseNumber * 5);
        }

        /// <summary>
        /// This function sets the production rate for building more combat units to be 1/comtohumanratio.
        /// </summary>
        public void ArmyDevelopment()
        {
            if (HumanArmyAssesment() == 0 || ComputerArmyAssesment() == 0)
            {
                return;
            }
            double comToHumanRatio = ComputerArmyAssesment() / HumanArmyAssesment();
            if (comToHumanRatio != 0)
            {
                this.productionRate = 1 / comToHumanRatio;    // This means: the stronger the ai is relative to the player, it will create less combat units.
            }
            else
            {
                return;  // would not build anything if the ratio is not valid. Means that the player has 0 units so its NULL. 
            }

            if (this.computerPlayer.playerResource > 0)
            {

            }
            else
            {
                return;   // would not build anything if resource is below 0. (Maybe we should think about having negative resource in cost of damage.)
            }
        }

        /// <summary>
        /// Takes an object and sends all combat units that are not busy to attack it.
        /// </summary>
        /// <param name="targetObject"></param>
        public void SendAllCombatUnitsTo(BasicGameObjectWithHp targetObject)
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is RangedUnit)
                {
                    RangedUnit unit = o as RangedUnit;
                    if (!unit.Busy)
                    {
                        this.OrderRangedUnitToAttack(unit, targetObject);
                    }
                }
                else if (o is MeleeUnit)
                {
                    MeleeUnit unit = o as MeleeUnit;
                    if (!unit.Busy)
                    {
                        this.OrderMeleeUnitToAttack(unit, targetObject);
                    }
                }
            }
        }

        /// <summary>
        /// Returns the number of work units the computer player has as an int.
        /// </summary>
        /// <returns></returns>
        public int WorkerCounter()
        {
            int workerNum = 0;
            foreach (BasicGameObject obj in this.computerPlayer.playerObjects.ToList())
            {
                if (obj is WorkUnit)
                {
                    workerNum++;
                }
            }
            return workerNum;
        }

        /// <summary>
        /// Returns the number of available work units the computer player has as an int.
        /// </summary>
        /// <returns></returns>
        public int AvailableWorkerCounter()
        {
            int workerNum = 0;
            foreach (BasicGameObject obj in this.computerPlayer.playerObjects.ToList())
            {
                if (obj is WorkUnit)
                {
                    WorkUnit worker = obj as WorkUnit;
                    if (!worker.Busy)
                    {
                        workerNum++;
                    }
                }
            }
            return workerNum;
        }

        /// <summary>
        /// Returns the number of work units that are collecting.
        /// </summary>
        /// <returns></returns>
        public int WorkersCollecting()
        {
            int workerNum = 0;
            foreach (BasicGameObject obj in this.computerPlayer.playerObjects.ToList())
            {
                if (obj is WorkUnit)
                {
                    WorkUnit worker = obj as WorkUnit;
                    if (worker.Busy && worker.isCollectResource)
                    {
                        workerNum++;
                    }
                }
            }
            return workerNum;
        }
        
        /// <summary>
        /// returns the number of the computer's available combat units as an int.
        /// </summary>
        /// <returns></returns>
        public int AvailableCombatUnits()
        {
            int available = 0;
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is RangedUnit || o is MeleeUnit)
                {
                    Unit u = o as Unit;
                    if (!u.Busy)
                    {
                        available++;
                    }
                }
            }
            return available;
        }

        public Base GetHumanBase()
        {
            foreach (BasicGameObject o in this.humanPlayer.playerObjects.ToList())
            {
                if (o is Base)
                {
                    Base humanBase = o as Base;
                    return humanBase;
                }
            }
            return null;
        }
        /// <summary>
        /// Needs to be explained.
        /// </summary>
        /// <param name="unit"></param>
        /// <param name="target"></param>
        public void OrderRangedUnitToAttack(RangedUnit unit, BasicGameObjectWithHp target)
        {
            unit.SetAttack(target);
        }

        public void OrderMeleeUnitToAttack(MeleeUnit unit, BasicGameObjectWithHp target)
        {
            unit.SetAttack(target);
        }

        public bool HaveBarracks()
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is Barracks)
                {
                    return true;
                }
            }
            return false;
        }

        public Barracks GetBarracks()
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is Barracks)
                {
                    Barracks barracks = o as Barracks;
                    return barracks;
                }
            }
            return null;
        }


        public WorkUnit GetAvailableWorker()   // does this get() func makes the unit busy? 
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is WorkUnit)
                {
                    WorkUnit unit = o as WorkUnit;
                    if (!unit.Busy)
                    {
                        return unit;
                    }
                }
            }
            return null;
        }

        public bool HaveAvailableWorker()
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is WorkUnit)
                {
                    WorkUnit unit = o as WorkUnit;
                    if (!unit.Busy)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Makes a work unit build a building by setting all needed fields.
        /// Takes a work unit to set, a type of building and a vector2 of the build position.
        /// </summary>
        /// <param name="workunit"></param>
        /// <param name="buildingType"></param>
        /// <param name="buildPos"></param>
        public void OrderWorkUnitToBuild(WorkUnit workunit, string buildingType, Vector2 buildPos)
        {
            workunit.SetWorkToBuild(buildingType, buildPos);
        }

        public void OrderBaseToCreateWorker(Base computerBase)
        {
            if (computerBase != null)
            {
                computerBase.CreateWorker();
            }
        }

        /// <summary>
        /// Returns the first base found on the list. 
        /// </summary>
        /// <returns></returns>
        public Base GetComputerBase()
        {
            foreach (BasicGameObject obj in this.computerPlayer.playerObjects.ToList())
            {
                if (obj is Base)
                {
                    return obj as Base;
                }
            }
            return null;
        }

        public void OrderUnitToMove(Unit unit, Vector2 Pos)
        {
            unit.SetUnitDestination(Pos);
        }

        /// <summary>
        /// Takes a barracks and calls its BuildUnits() function with a taken UnitType parameter.
        /// </summary>
        /// <param name="barracks"></param>
        /// <param name="UnitType"></param>
        public void OrderBarracksToCreate(Barracks barracks, string UnitType)
        {
            barracks.BuildUnits(UnitType);
        }

        /// <summary>
        /// !!
        /// </summary>
        /// <param name="workunit"></param>
        /// <param name="targetResource"></param>
        public void OrderUnitToCollect(WorkUnit workunit, Resource targetResource)
        {
            if (targetResource != null && workunit != null)
            {
                workunit.SetToCollect(targetResource);
            }
        }
        public void OrderMultipleWorkersToCollect(int workers, Resource targetResource)
        {

        }
        /// <summary>
        /// !!
        /// </summary>
        /// <param name="unit"></param>
        public void SetWorkUnitToIdeal(WorkUnit unit)
        {
            unit.isCollectResource = false;
            unit.checkMove = true;
            unit.isRepair = false;
            unit.moveToBuild = false;
            unit.reactionNeeded = false;
        }
        /// <summary>
        /// !!
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public Resource FindResourceNearByUnit(List<BasicGameObject> objectList,WorkUnit unit)
        {
            if (unit != null)
            {
                float minimalVectorLength = float.PositiveInfinity; // cool
                Resource returnedResource = null;
                foreach (BasicGameObject o in objectList.ToList())
                {
                    if (o is Resource)
                    {
                        Resource r = o as Resource;
                        float distance = (-unit.GetMiddleVector() + r.GetMiddleVector()).Length();
                        if (minimalVectorLength > distance)
                        {
                            returnedResource = r;
                            minimalVectorLength = distance;
                        }
                    }
                }

                return returnedResource;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Needs to be written!!
        /// </summary>
        /// <returns></returns>
        public BasicGameObjectWithHp SeekTarget()
        {
            foreach (BasicGameObject o in this.humanPlayer.playerObjects.ToList())
            {
                if (o is BasicGameObjectWithHp)
                {
                    BasicGameObjectWithHp objhp = o as BasicGameObjectWithHp;
                    if (!objhp.targeted)
                    {
                        objhp.targeted = true;
                        return objhp;
                    }
                }
            }
            return null;
        }

        public RangedUnit GetRangedUnit()
        {
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is RangedUnit)
                {
                    RangedUnit unit = o as RangedUnit;

                    return unit;

                }
            }
            return null;
        }

        // ----------------------------------------------------------- States and Assesments -----------------------------------------------

        /// <summary>
        /// This tells the AI manager what he should be "feeling".
        /// It knows the ratio between the computer and the human armies' strengh and can set itself in 3 ways.
        /// </summary>
        /// <returns></returns>
        public string StateAssesment()
        {
            if (HumanArmyAssesment() == 0 && ComputerArmyAssesment() > 0)
            {
                return "Neutral";
            }
            else if (HumanArmyAssesment() == 0 & ComputerArmyAssesment() == 0)
            {
                return "Start";
            }
            double comToHumanRatio = ComputerArmyAssesment() / HumanArmyAssesment();
            if (comToHumanRatio >= 3)
            {
                return "Aggresive";
            }
            else if (comToHumanRatio <= 0.2)
            {
                return "Defensive";
            }
            return "Neutral"; // In any other case.

        }


        /// <summary>
        /// Goes over every object in the HUMAN player objects' list and checks the kind of each object.
        /// Adds points to the total "strengh" it rates an army to be. Each type of unit provide different amount of points.
        /// </summary>
        /// <returns></returns>
        public int HumanArmyAssesment()
        {
            int humanArmyStr = 0;
            foreach (BasicGameObject o in this.humanPlayer.playerObjects.ToList())
            {
                if (o is CombatUnit)
                {
                    humanArmyStr++;
                }
            }
            return humanArmyStr;
        }


        /// <summary>
        /// Goes over every object in the COMPUTER player objects' list and checks the kind of each object.
        /// Adds points to the total "strengh" it rates an army to be. Each type of unit provide different amount of points.
        /// </summary>
        /// <returns></returns>
        public int ComputerArmyAssesment()
        {
            int computerArmyStr = 0;
            foreach (BasicGameObject o in this.computerPlayer.playerObjects.ToList())
            {
                if (o is CombatUnit)
                {
                    computerArmyStr++;
                }
            }
            return computerArmyStr;
        }
    }
}
