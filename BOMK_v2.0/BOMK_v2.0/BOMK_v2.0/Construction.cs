﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOMK_v2._0
{
    public class Construction
    {
        public string typeOfConstruction;
        public int timeLeftToFinish;
        public WorkUnit workunit;
        public Construction(WorkUnit workunit)
        {
            this.workunit = workunit;
        }
    }
}
