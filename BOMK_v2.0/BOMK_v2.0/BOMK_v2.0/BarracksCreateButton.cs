﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace BOMK_v2._0
{
    class BarracksCreateButton : BasicButton
    {
        public BarracksCreateButton(Vector2 drawPos, Texture2D texture, Game1 game,BasicGameObject linkedObject)
            : base(drawPos, texture, game,linkedObject)
        {

        }
        protected override void OnClick(Point mousePosition)
        {
            WorkUnit workUnit = this.linkedObject as WorkUnit;
            workUnit.reactionNeeded = true;
            workUnit.typeOfBuildingWanted = "Barracks";
            workUnit.passedUpdatesSinceCommand = 0;
        }
    }
}
