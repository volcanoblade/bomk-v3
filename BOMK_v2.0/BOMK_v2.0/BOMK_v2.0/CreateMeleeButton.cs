﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    class CreateMeleeButton : BasicButton
    {
        public CreateMeleeButton(Vector2 drawPos, Texture2D texture, Game1 game, BasicGameObject linkedBuilding)
            : base(drawPos, texture, game, linkedBuilding)
        {

        }
        protected override void OnClick(Point mousePosition)
        {
            if (this.GetDisplayRectangle().Contains(mousePosition))
            {
                Vector2 createdVector = new Vector2(600, 100);
                Texture2D createdTexture = game.Content.Load<Texture2D>("BlueMeleeUnit");
                MeleeUnit createdUnit = new MeleeUnit(createdVector, createdTexture, this.game);
                game.AddToObjectList(createdUnit);
            }
        }
    }
}
