﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    public class MainMenu
    {
        MainMenuButton startButton;
        MainMenuButton helpButton;
        MainMenuButton quitButton;
        public Game1 game;
        public string typeOfFocusedButton;
        MainMenuButton currentButton;
        public int timePassedSinceLastClick = 0;
        public bool inHelpMenu;
        /// <summary>
        /// Constructor.
        /// </summary>
        public MainMenu(Game1 game)
        {
            this.game = game;
            this.typeOfFocusedButton = "Start";
            this.startButton = new MainMenuButton(new Vector2(this.game.screenWidth/2 - 100, 100), this.game, this.game.Content.Load<Texture2D>("FocusedStartButton"), "StartButton");
            this.helpButton = new MainMenuButton(new Vector2(this.game.screenWidth / 2 - 100, 300), this.game, this.game.Content.Load<Texture2D>("HelpButton"), "HelpButton");
            this.quitButton = new MainMenuButton(new Vector2(this.game.screenWidth / 2 - 100, 500), this.game, this.game.Content.Load<Texture2D>("QuitButton"), "QuitButton");
            this.startButton.nextButton = helpButton;
            this.startButton.prevButton = quitButton;
            this.helpButton.nextButton = quitButton;
            this.helpButton.prevButton = startButton;
            this.quitButton.nextButton = startButton;
            this.quitButton.prevButton = startButton;
            this.currentButton = startButton;
        }

        public void MainMenuBehaviour()
        {
            KeyboardState newState = Keyboard.GetState();
            if (timePassedSinceLastClick < 10)
            {
                timePassedSinceLastClick++;
                return;
            }   
            if (this.typeOfFocusedButton=="Start")
            {
                if (newState.IsKeyDown(Keys.Up))
                {
                    this.typeOfFocusedButton = "Quit";
                    this.startButton.AdjustTexture(false);
                    this.quitButton.AdjustTexture(true);
                    timePassedSinceLastClick = 0;
                }
                else if (newState.IsKeyDown(Keys.Down))
                {
                    this.typeOfFocusedButton = "Help";
                    this.startButton.AdjustTexture(false);
                    this.helpButton.AdjustTexture(true);
                    timePassedSinceLastClick = 0;
                }
            }
            else if (this.typeOfFocusedButton=="Help")
            {
                if (newState.IsKeyDown(Keys.Up))
                {
                    this.typeOfFocusedButton = "Start";
                    this.helpButton.AdjustTexture(false);
                    this.startButton.AdjustTexture(true);
                    timePassedSinceLastClick = 0;
                }
                else if (newState.IsKeyDown(Keys.Down))
                {
                    this.typeOfFocusedButton = "Quit";
                    this.helpButton.AdjustTexture(false);
                    this.quitButton.AdjustTexture(true);
                    timePassedSinceLastClick = 0;
                }          
            }
            else if (this.typeOfFocusedButton=="Quit")
            {
                if (newState.IsKeyDown(Keys.Up))
                {
                    this.typeOfFocusedButton = "Help";
                    this.quitButton.AdjustTexture(false);
                    this.helpButton.AdjustTexture(true);
                    timePassedSinceLastClick = 0;
                }
                else if (newState.IsKeyDown(Keys.Down))
                {
                    this.typeOfFocusedButton = "Start";
                    this.quitButton.AdjustTexture(false);
                    this.startButton.AdjustTexture(true);
                    timePassedSinceLastClick = 0;
                }
            }
        }

        public void Draw()
        {
            if (this.inHelpMenu)
                return;
            this.startButton.Draw(this.game.spriteBatch);
            this.helpButton.Draw(this.game.spriteBatch);
            this.quitButton.Draw(this.game.spriteBatch);
            this.game.DrawMenuGuide();
        }

    }
}
