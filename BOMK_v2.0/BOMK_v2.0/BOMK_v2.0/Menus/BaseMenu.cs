﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    class BaseMenu : Menu
    {
        protected WorkButton workUnitButton;
        protected DestroyButton destroyButton;
        public BaseMenu(Vector2 drawPos, Texture2D texture, Game1 game, BasicGameObject linkedObject, Player connetcedPlayer)
            : base(drawPos, texture, game, linkedObject, connetcedPlayer)
        {
            this.linkedObject = linkedObject;
        }
        public void createMenu()
        {
            // Creation of the three texture draw vectors. Each texture width is 95f -> 0, 95, 190 on the x axis
            Vector2 workButtonVector = new Vector2(this.game.cameraPos.X, this.game.screenHeight - 100 + this.game.cameraPos.Y);
            workUnitButton = new WorkButton(workButtonVector, game.Content.Load<Texture2D>("WorkCreateButton"), game, this.linkedObject, this.connectedPlayer);
            this.connectedPlayer.AddObjectToPlayer(workUnitButton);
            destroyButton = new DestroyButton(new Vector2(95 + this.game.cameraPos.X, this.game.screenHeight - 100 + this.game.cameraPos.Y), this.game.Content.Load<Texture2D>("DestroyButton"), game, this.linkedObject, this.connectedPlayer);
            this.connectedPlayer.AddObjectToPlayer(destroyButton);
        }
    }
}