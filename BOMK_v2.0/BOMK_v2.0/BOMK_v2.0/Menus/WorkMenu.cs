﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace BOMK_v2._0
{
    class WorkMenu : Menu
    {
        private BaseCreateButton baseCreateButton;
        private BarrakcsCreateButton barracksCreateButton;
        private FarmCreateButton farmCreateButton;
        private TowerCreateButton towerCreateButton;
        protected DestroyButton destroyButton;
        public WorkMenu(Vector2 drawPos, Texture2D texture, Game1 game, WorkUnit linkedWorkUnit, Player connectedPlayer)
            : base(drawPos, texture, game, linkedWorkUnit, connectedPlayer)
        {

        }
        public void createMenu()
        {
            Vector2 baseCreateButtonVector = new Vector2(this.game.cameraPos.X, this.game.screenHeight - 100 + this.game.cameraPos.Y);
            Vector2 barracksCreateButtonVector = new Vector2(this.game.cameraPos.X + 95, this.game.screenHeight - 100 + this.game.cameraPos.Y);
            Vector2 farmCreateButtonVector = new Vector2(this.game.cameraPos.X + 190, this.game.screenHeight - 100 + this.game.cameraPos.Y);
            Vector2 towerCreateButtonVector = new Vector2(this.game.cameraPos.X + 285, this.game.screenHeight - 100 + this.game.cameraPos.Y);
            baseCreateButton = new BaseCreateButton(baseCreateButtonVector, game.Content.Load<Texture2D>("BaseCreateButton"), game, this.linkedObject, this.connectedPlayer);
            barracksCreateButton = new BarrakcsCreateButton(barracksCreateButtonVector, game.Content.Load<Texture2D>("BarracksCreateButton"), game, this.linkedObject, this.connectedPlayer);
            farmCreateButton = new FarmCreateButton(farmCreateButtonVector, game.Content.Load<Texture2D>("FarmCreateButton"), game, this.linkedObject, this.connectedPlayer);
            towerCreateButton = new TowerCreateButton(towerCreateButtonVector, game.Content.Load<Texture2D>("TowerCreateButton"), game, this.linkedObject, this.connectedPlayer);
            game.AddToObjectList(baseCreateButton);
            game.AddToObjectList(barracksCreateButton);
            game.AddToObjectList(farmCreateButton);
            game.AddToObjectList(towerCreateButton);
            destroyButton = new DestroyButton(new Vector2(380 + this.game.cameraPos.X, this.game.screenHeight - 100 + this.game.cameraPos.Y), this.game.Content.Load<Texture2D>("DestroyButton"), game, this.linkedObject, this.connectedPlayer
              );
            this.connectedPlayer.AddObjectToPlayer(destroyButton);
        }
    }
}