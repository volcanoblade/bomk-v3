﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    class BarracksMenu : Menu
    {
        protected RangedButton rangedUnitButton;
        protected MeleeButton meleeUnitButton;
        protected DestroyButton destroyButton;
        public BarracksMenu(Vector2 drawPos, Texture2D texture, Game1 game, BasicGameObject linkedObject, Player connectedPlayer)
            : base(drawPos, texture, game, linkedObject, connectedPlayer)
        {

        }
        public void createMenu()
        {
            // Creation of the three texture draw vectors. Each texture width is 95f -> 0, 95
            Vector2 RangedButtonVector = new Vector2(this.game.cameraPos.X, this.game.screenHeight - 100 + this.game.cameraPos.Y);
            Vector2 meleeButtonVector = new Vector2(this.game.cameraPos.X + 95, this.game.screenHeight - 100 + this.game.cameraPos.Y);
            rangedUnitButton = new RangedButton(RangedButtonVector, game.Content.Load<Texture2D>("RangedCreateButton"), game, this.linkedObject, this.connectedPlayer);
            meleeUnitButton = new MeleeButton(meleeButtonVector, game.Content.Load<Texture2D>("MeleeCreateButton"), game, this.linkedObject, this.connectedPlayer);
            destroyButton = new DestroyButton(new Vector2(190 + this.game.cameraPos.X, this.game.screenHeight - 100 + this.game.cameraPos.Y), this.game.Content.Load<Texture2D>("DestroyButton"), game, this.linkedObject, this.connectedPlayer);
            this.connectedPlayer.AddObjectToPlayer(destroyButton);
            game.AddToObjectList(rangedUnitButton);
            game.AddToObjectList(meleeUnitButton);
        }
    }
}