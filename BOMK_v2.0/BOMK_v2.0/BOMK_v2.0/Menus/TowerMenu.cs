﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    class TowerMenu : Menu
    {
        public DestroyButton destroyButton;
        public TowerMenu(Vector2 drawPos, Texture2D texture, Game1 game, BasicGameObject linkedObject, Player connectedPlayer)
            : base(drawPos, texture, game, linkedObject, connectedPlayer)
        {
            this.CreateMenu();
        }
        public void CreateMenu()
        {
            destroyButton = new DestroyButton(new Vector2(this.game.cameraPos.X, this.game.screenHeight - 100 + this.game.cameraPos.Y), this.game.Content.Load<Texture2D>("DestroyButton"), game, this.linkedObject, this.connectedPlayer);
            this.connectedPlayer.AddObjectToPlayer(destroyButton);
        }

    }
}
