﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    class MeleeButton : BasicButton
    {
        private Barracks linkedBarracks;
        public string type = "Melee";
        public MeleeButton(Vector2 drawPos, Texture2D texture, Game1 game, BasicGameObject linkedBuilding, Player connectedPlayer)
            : base(drawPos, texture, game, linkedBuilding, connectedPlayer)
        {
            linkedBarracks = this.linkedObject as Barracks;
        }
        protected override void OnClick(Point mousePosition)
        {
            this.linkedBarracks.SetIsFocused(true);
            if (this.connectedPlayer.playerResource >= this.game.meleeCost && this.connectedPlayer.IsPlayerBuildingUsageBelowCap())
            {
                this.linkedBarracks.BuildUnits(type);
            }
            else if (this.connectedPlayer.playerResource < this.game.meleeCost)
            {
                this.game.EnableError("You can't build that. Not enough Resources");
            }
            else if (!this.connectedPlayer.IsPlayerBuildingUsageBelowCap())
            {
                this.game.EnableError("You can't build that. Build capacity reached. Build more houses");
            }
        }
    }
}
