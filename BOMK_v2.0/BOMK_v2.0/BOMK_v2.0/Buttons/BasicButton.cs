﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace BOMK_v2._0
{
    class BasicButton : BasicGameObject// as of a genreal button. not for base building
    {
        protected BasicGameObject linkedObject;
        public BasicButton(Vector2 drawPos, Texture2D texture, Game1 game, BasicGameObject linkedObject,Player connectedPlayer) : base(drawPos,texture,game,connectedPlayer)
        {
            this.linkedObject = linkedObject;
        }

        /// <summary>
        /// Overrided function from BasicGameObject. Check if this button's linked object is focused by the player.
        /// If it is, then it draws it on the screen.
        /// If it is not, then it removes it from the game.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            BasicGameObjectWithHp owner = this.linkedObject as BasicGameObjectWithHp;
            if (this.linkedObject.GetIsFocused())
            {
                base.Draw(spriteBatch);
            }
            else
            {
                owner.hasMenu = false;
                this.connectedPlayer.RemoveObjectFromPlayer(this);
            }
        }

        /// <summary>
        /// Called by ObjectBehaviour()
        /// Checks if the object that this button belongs to is still "alive".
        /// If it does, the function removes it's button from the game.
        /// </summary>
        public void ButtonCheck()
        {
            if (this.linkedObject is BasicGameObjectWithHp)
            {
                BasicGameObjectWithHp o = this.linkedObject as BasicGameObjectWithHp;
                if (o.GetHp() <= 0)
                {
                    this.connectedPlayer.RemoveObjectFromPlayer(this);
                }
            }
        }
        public void ButtonFollow()
        {
            this.realPos += this.game.movingDirection;
        }
        protected override void OnClick(Point mousePosition)
        {
            
        }
    }
}
