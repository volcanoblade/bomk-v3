﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    class WorkButton : BasicButton
    {
        public WorkButton(Vector2 drawPos, Texture2D texture, Game1 game, BasicGameObject linkedBuilding,Player connectedPlayer)
            : base(drawPos, texture, game, linkedBuilding,connectedPlayer)
        {

        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            Base b = this.linkedObject as Base;
            if (b.GetIsFocused())
            {
                base.Draw(spriteBatch);
            }
            else
            {
                b.hasMenu = false;
                this.connectedPlayer.RemoveObjectFromPlayer(this);
            }
        }
        protected override void OnClick(Point mousePosition)
        {
            this.linkedObject.SetIsFocused(true);
            if (this.GetDisplayRectangle().Contains(mousePosition))
            {
                if (this.connectedPlayer.playerResource >= this.game.workerCost && this.connectedPlayer.IsPlayerBuildingUsageBelowCap())
                {
                    Base connectedBase = this.linkedObject as Base;
                    connectedBase.CreateWorker();
                }
                else if (this.connectedPlayer.playerResource < this.game.workerCost)
                {
                    this.game.EnableError("You can't build that. Not enough Resources");
                }
                else if (!this.connectedPlayer.IsPlayerBuildingUsageBelowCap())
                {
                    this.game.EnableError("You can't build that. Build capacity reached. Build more houses");
                }
                
            }
        }
    }
}
