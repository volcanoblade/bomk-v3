﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    public class MainMenuButton 
    {
       
        public Vector2 pos;
        public Game1 game;
        public Texture2D texture;
        public string type;
        public MainMenuButton nextButton;
        public MainMenuButton prevButton;

         /// <summary>
        /// Constructor.
        /// </summary>
        public MainMenuButton(Vector2 pos, Game1 game,Texture2D texture,string type)
        {
            this.pos = pos;
            this.game = game;
            this.texture = texture;
            this.type = type;
        }

        public void AdjustTexture(bool focused)
        {
            if (focused)
            {
                this.texture = this.game.Content.Load<Texture2D>("Focused" + type);
            }
            else
	        {
                this.texture = this.game.Content.Load<Texture2D>(type);
	        }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, this.pos, Color.White);
        }

    }
}
