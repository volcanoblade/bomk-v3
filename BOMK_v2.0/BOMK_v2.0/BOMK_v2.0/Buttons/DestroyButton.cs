﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    class DestroyButton : BasicButton
    {
        public DestroyButton(Vector2 drawPos, Texture2D texture, Game1 game, BasicGameObject linkedObject, Player connectedPlayer)
            : base(drawPos, texture, game, linkedObject, connectedPlayer)
        {

        }
        protected override void OnClick(Point mousePosition)
        {
            if(linkedObject is CombatUnit)
            this.connectedPlayer.buildingUsage--;
            this.connectedPlayer.RemoveObjectFromPlayer(this.linkedObject);
        }
    }
}
