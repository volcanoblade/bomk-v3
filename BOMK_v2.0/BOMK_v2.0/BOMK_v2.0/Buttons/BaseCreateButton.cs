﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    class BaseCreateButton : BasicButton
    {
        public BaseCreateButton(Vector2 drawPos, Texture2D texture, Game1 game, BasicGameObject linkedObject, Player connectedPlayer)
            : base(drawPos, texture, game, linkedObject, connectedPlayer)
        {

        }
        protected override void OnClick(Point mousePosition)
        {
            WorkUnit workUnit = this.linkedObject as WorkUnit;
            workUnit.SetIsFocused(true);
            if (workUnit.connectedPlayer.playerResource >= 20)
            {
                workUnit.reactionNeeded = true;
                workUnit.passedUpdatesSinceCommand = 0;
                workUnit.typeOfBuildingWanted = "Base";
                Vector2 imaginaryBuildingPosition = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
                workUnit.connectedImaginaryBuilding = new ImaginaryBuilding(imaginaryBuildingPosition, this.game.Content.Load<Texture2D>("ImaginaryBarracks"), this.game, this.connectedPlayer);
                this.connectedPlayer.AddObjectToPlayer(workUnit.connectedImaginaryBuilding);
            }

        }
    }
}
