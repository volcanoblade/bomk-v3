﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    public class Player
    {
        public List<BasicGameObject> playerObjects;
        public int playerResource;
        public int buildingUsage;
        public int buildingCapacity;
        public bool isHumanPlayer;
        private Game1 game;
        public AIManager computerAI;
        public int damageInflictied;
        public Player(Game1 game, string kindOfPlayer)
        {
            this.game = game;
            this.playerResource = 100;
            this.playerObjects = new List<BasicGameObject>();

            if (kindOfPlayer == "Human")
            {
                this.isHumanPlayer = true;
                this.playerResource = 200;
                Vector2 starterBasePosition = new Vector2(100, 100);
                Base playerStarterBase = new Base(starterBasePosition, this.game.Content.Load<Texture2D>("BlueBase"), this.game, this);
                AddObjectToPlayer(playerStarterBase);
                this.buildingCapacity = 5;
            }
            else if (kindOfPlayer == "Computer")
            {
                this.isHumanPlayer = false;
                Vector2 starterBasePosition = new Vector2(1000, 1000);
                Base playerStarterBase = new Base(starterBasePosition, this.game.Content.Load<Texture2D>("RedBase"), this.game, this);
                AddObjectToPlayer(playerStarterBase);
                this.buildingCapacity = 5;
            }
        }

        /// <summary>
        /// This function creates an actual instance of AIManager and will be called only once - in the game's initialize function.
        /// It is a function because AIManager needs the human player as a parameter in his constructor.
        /// </summary>
        public void AIManagerCreateFunction(Player humanPlayer, Game1 game)
        {
            computerAI = new AIManager(this, humanPlayer, this.game);
        }


        public bool IsPlayerBuildingUsageBelowCap()
        {
            if (this.buildingUsage < this.buildingCapacity)
                return true;
            else
                return false;
        }

        public bool CheckIfLost()
        {
            int numOfBases = 0;
            foreach (BasicGameObject obj in this.playerObjects.ToList())
            {
                if (obj is Base)
                {
                    numOfBases++;
                }
            }
            if (numOfBases == 0)
            {
                this.game.gameEnded = true;
                if (this.isHumanPlayer)
                {
                    this.game.losingPlayer = "Human";
                }
                else
                {
                    this.game.losingPlayer = "Computer";
                }

                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets an object and adds it to the players objects and to the game's list of objects.
        /// </summary>
        /// <param name="obj"></param>
        public void AddObjectToPlayer(BasicGameObject obj)
        {
            this.playerObjects.Add(obj);
            this.game.AddToObjectList(obj);
        }
        /// <summary>
        /// Gets an object and removes it from the players objects and from the game's list of objects.
        /// </summary>
        /// <param name="obj"></param>
        public void RemoveObjectFromPlayer(BasicGameObject obj)
        {
            this.playerObjects.Remove(obj);
            this.game.RemoveFromObjectList(obj);
        }
        public void addSupply()
        {
            this.playerResource++;
        }




    }
}
