﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    public class WorkUnit : Unit
    {
        public bool moveToBuild;
        public Vector2 buildPosition;
        public bool reactionNeeded;
        public bool isCollectResource;
        public int passedUpdatesSinceCommand;
        public int timeToBuild;
        public bool finished = true;
        private Building buildingTarget;
        private Resource resourceTarget;
        public string typeOfBuildingWanted;
        public bool isRepair = false;
        public ImaginaryBuilding connectedImaginaryBuilding;
        public SoundEffect jobsDone;

        public WorkUnit(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game, connectedPlayer)
        {
            this.type = "WorkUnit";
            this.buildCost = this.game.workerCost;
            this.attackSpeed = 0.1f;
            this.range = 5;
            this.SetHp(100);
            this.connectedPlayer.playerResource = this.connectedPlayer.playerResource - this.buildCost;
            this.jobsDone = this.game.Content.Load<SoundEffect>("jobsdone");
            this.connectedPlayer.buildingUsage++;
        }
        public void SetToMove()
        {
            this.checkMove = true;
            this.isRepair = false;
            this.isCollectResource = false;
            this.moveToBuild = false;
            this.reactionNeeded = false;
            this.Busy = true;
        }
        public void SetToCollect()
        {
            this.isCollectResource = true;
            this.isRepair = false;
            this.checkMove = false;
            this.moveToBuild = false;
            this.reactionNeeded = false;
            this.Busy = true;
        }
        public void SetToBuild()
        {
            this.moveToBuild = true;
            this.checkMove = false;
            this.isCollectResource = false;
            this.isRepair = false;
            this.reactionNeeded = false;
        }
        public void SetToRepair()
        {
            this.isRepair = true;
            this.checkMove = false;
            this.moveToBuild = false;
            this.isCollectResource = false;
            this.reactionNeeded = false;
        }
        public override void UnitBehaviour()
        {
            MouseState mousestate = Mouse.GetState();
            Point mousePosition = new Point(mousestate.X, mousestate.Y);
            this.BuildBehaviour();
            if (this.isFocused && mousestate.RightButton == ButtonState.Pressed && this.connectedPlayer.isHumanPlayer)
            {
                bool objectClicked = false;
                foreach (BasicGameObject o in game.GetObjectList())
                {
                    if (o.GetDisplayRectangle().Contains(mousePosition) && o is Building && !(o.connectedPlayer.isHumanPlayer))
                    {
                        this.unitDestination.X = mousestate.X + this.game.cameraPos.X;
                        this.unitDestination.Y = mousestate.Y + this.game.cameraPos.Y;
                        this.SetToMove();
                        objectClicked = true;
                    }
                    if (o.GetDisplayRectangle().Contains(mousePosition) && (o is Building))
                    {
                        this.buildingTarget = o as Building;
                        this.SetToRepair();
                        objectClicked = true;
                    }

                    else if (o.GetDisplayRectangle().Contains(mousePosition) && o is Resource)  // Means that the unit wants to collect a resource.
                    {                                                                           // It sets all other flags to false to indicate it now collects and does not attack/move.
                        this.resourceTarget = o as Resource;
                        this.SetToCollect();
                        objectClicked = true;
                    }
                }
                if (!objectClicked)  // The click was not on any object.
                {
                    this.unitDestination.X = mousestate.X + this.game.cameraPos.X;
                    this.unitDestination.Y = mousestate.Y + this.game.cameraPos.Y;
                    this.SetToMove();
                }
            }
            this.Collision();
            this.Repair(this.buildingTarget);
            this.CollectResource(resourceTarget);
            this.Move();
        }
        /// <summary>
        /// Handles of all the reactions that the player gives in order to build buildings.
        /// Called every game time and builds according to information given by a button press.
        /// </summary>
        /// 

        public void SetWorkUnitToCollect(Resource r)
        {
            this.resourceTarget = r;
            this.isCollectResource = true;
            this.checkMove = false;
            this.isRepair = false;
            this.moveToBuild = false;
            this.reactionNeeded = false;
        }

        public void BuildBehaviour()
        {
            MouseState mouseState = Mouse.GetState();
            bool canBeBuilt = true;
            if (reactionNeeded && Mouse.GetState().LeftButton == ButtonState.Pressed && passedUpdatesSinceCommand > 10)
            {
                foreach (BasicGameObject o in this.game.GetObjectList())
                {
                    if (o != null && o != this.connectedImaginaryBuilding)
                    {
                        if (o.GetDisplayRectangle().Intersects(this.connectedImaginaryBuilding.GetDisplayRectangle()))
                        {
                            canBeBuilt = false;
                        }
                    }
                }
                if (canBeBuilt)
                {
                    this.SetIsFocused(true);
                    this.timeToBuild = 100;
                    this.SetToBuild();
                    buildPosition.X = mouseState.X + this.game.cameraPos.X;
                    buildPosition.Y = mouseState.Y + this.game.cameraPos.Y;
                    this.connectedImaginaryBuilding.Delete();
                }
            }
            else if (reactionNeeded && Mouse.GetState().RightButton == ButtonState.Pressed && passedUpdatesSinceCommand > 10)
            {
                this.game.SetFocusedObject(this);
                this.isFocused = true;
                this.connectedImaginaryBuilding.Delete();
                this.SetToMove();
            }
            else
            {
                passedUpdatesSinceCommand++;
            }
            Vector2 distance = buildPosition - this.GetMiddleVector();
            if (this.moveToBuild)
            {
                finished = false;
                if (distance.Length() <= this.GetCircleRadius().Length())
                {
                    if (this.typeOfBuildingWanted == "Barracks")
                    {
                        if (this.timeToBuild <= 0)
                        {
                            if (this.connectedPlayer.playerResource > this.game.barracksCost)
                            {
                                string textureString;
                                if (this.connectedPlayer.isHumanPlayer)
                                    textureString = "BlueBarracks";
                                else
                                    textureString = "RedBarracks";
                                Barracks createdBarracks = new Barracks(new Vector2(buildPosition.X, buildPosition.Y), this.game.Content.Load<Texture2D>(textureString), this.game, this.connectedPlayer);
                                this.connectedPlayer.AddObjectToPlayer(createdBarracks);
                                this.finished = true;
                                if (this.connectedPlayer.isHumanPlayer)
                                    jobsDone.Play();
                            }
                            else
                            {
                                this.finished = true;
                            }

                        }
                        else
                        {
                            this.timeToBuild--;
                        }
                        
                    }
                    else if (this.typeOfBuildingWanted == "Base")
                    {
                        if (this.timeToBuild <= 0)
                        {
                            if (this.connectedPlayer.playerResource > this.game.barracksCost)
                            {
                                string textureString;
                                if (this.connectedPlayer.isHumanPlayer)
                                    textureString = "BlueBase";
                                else
                                    textureString = "RedBase";
                                Base createdBase = new Base(new Vector2(buildPosition.X, buildPosition.Y), this.game.Content.Load<Texture2D>(textureString), this.game, this.connectedPlayer);
                                this.connectedPlayer.AddObjectToPlayer(createdBase);
                                this.finished = true;
                                if (this.connectedPlayer.isHumanPlayer)
                                    jobsDone.Play();
                            }
                            else
                            {
                                this.finished = true;
                            }

                        }
                        else
                        {
                            this.timeToBuild--;
                        }

                    }
                    else if (this.typeOfBuildingWanted == "Tower")
                    {
                        if (this.timeToBuild <= 0)
                        {
                            if (this.connectedPlayer.playerResource > this.game.barracksCost)
                            {
                                string textureString;
                                if (this.connectedPlayer.isHumanPlayer)
                                    textureString = "BlueTower";
                                else
                                    textureString = "RedTower";
                                Tower createdTower = new Tower(new Vector2(buildPosition.X, buildPosition.Y), this.game.Content.Load<Texture2D>(textureString), this.game, this.connectedPlayer);
                                this.connectedPlayer.AddObjectToPlayer(createdTower);
                                this.finished = true;
                                if (this.connectedPlayer.isHumanPlayer)
                                    jobsDone.Play();
                            }
                            else
                            {
                                this.finished = true;
                            }

                        }
                        else
                        {
                            this.timeToBuild--;
                        }

                    }
                    else if (this.typeOfBuildingWanted == "House")
                    {
                        if (this.timeToBuild <= 0)
                        {
                            if (this.connectedPlayer.playerResource > this.game.barracksCost)
                            {
                                string textureString;
                                if (this.connectedPlayer.isHumanPlayer)
                                    textureString = "BlueHouse";
                                else
                                    textureString = "RedHouse";
                                House createdFarm = new House(new Vector2(buildPosition.X, buildPosition.Y), this.game.Content.Load<Texture2D>(textureString), this.game, this.connectedPlayer);
                                this.connectedPlayer.AddObjectToPlayer(createdFarm);
                                this.finished = true;
                                if (this.connectedPlayer.isHumanPlayer)
                                    jobsDone.Play();
                            }
                            else
                            {
                                this.finished = true;
                            }

                        }
                        else
                        {
                            this.timeToBuild--;
                        }

                    }
                    if (finished)
                    {
                        this.timeToBuild = 100;
                        passedUpdatesSinceCommand = 0;
                        typeOfBuildingWanted = "";
                        moveToBuild = false;
                        this.Busy = false;                        
                    }
                }
                else
                {
                    distance.Normalize();
                    this.realPos += distance;
                }
            }
        }


        /// <summary>
        /// Gets a tatrget as BaseGameObjectWithHp.
        /// Check if this unit is not its own target, if the target is defined and if this unit is supposed to Repair.
        /// Then, Checks if the target is within this units Repair range.
        /// If it does, it decreases 1 from the traget's hp value.
        /// If it does not, it goes toward it.
        /// </summary>
        /// <param name="target"></param>
        public virtual void Repair(Building target)
        {
            if (target != null && target.GetHp() < target.GetMaxHp() && isRepair && this.connectedPlayer.isHumanPlayer == target.connectedPlayer.isHumanPlayer)
            {
                this.Busy = true;
                timeSinceLastShot += this.attackSpeed;
                Vector2 distance = target.GetMiddleVector() - this.GetMiddleVector();
                if (this.GetCircleRadius().Length() + this.range + target.GetCircleRadius().Length() >= distance.Length())
                {
                    if (timeSinceLastShot >= 2f)
                    {
                        RepairProjectile shot = new RepairProjectile(this.game.Content.Load<Texture2D>("RepairRay"), this.GetMiddleVector(), this.game, target, this.connectedPlayer);
                        this.game.AddToObjectList(shot);
                        timeSinceLastShot = 0f;
                    }
                }
                else
                {
                    distance.Normalize();
                    this.realPos += distance;
                }
            }
            else // if ((target == null || target.GetHp() == target.GetMaxHp()) && isRepair)
            {
                this.isRepair = false;
                this.Busy = false;
            }
        }

        /// <summary>
        /// Called when ever a unit is interacting with a resource and "collects" from it.
        /// checks if this unit is collecting and if so adds resource to the player and takes from the resource supply.
        /// </summary>
        public void CollectResource(Resource target)
        {

            if (target != null && isCollectResource && this.game.IsExistInGameObjectList(target))
            {
                this.Busy = true;
                timeSinceLastShot += this.attackSpeed;
                Vector2 distance = target.GetMiddleVector() - this.GetMiddleVector();
                if (this.GetCircleRadius().Length() + this.range + target.GetCircleRadius().Length() >= distance.Length())
                {
                    if (timeSinceLastShot >= 2f)
                    {
                        target.ResourceTransaction(this);
                        timeSinceLastShot = 0f;
                    }
                }
                else
                {
                    distance.Normalize();
                    this.realPos += distance;
                }
            }
            else //if (target == null && isCollectResource)
            {
                this.isCollectResource = false;
                this.Busy = false;
            }
        }

        public void SetToCollect(Resource r)
        {
            this.resourceTarget = r;
            this.isCollectResource = true;
            this.checkMove = false;
            this.isRepair = false;
            this.moveToBuild = false;
            this.Busy = true;
        }
        public void SetToRepair(Building b)
        {
            this.buildingTarget = b;
            this.isRepair = true;
            this.checkMove = false;
            this.moveToBuild = false;
            this.isCollectResource = false;
            this.Busy = true;
        }

        protected override void OnClick(Point mousePosition)
        {
            if (!this.hasMenu)
            {
                Vector2 workMenuVector = new Vector2(0, 382);
                WorkMenu menu = new WorkMenu(workMenuVector, this.game.Content.Load<Texture2D>("MenuEmptyTexture"), this.game, this, this.connectedPlayer);
                menu.createMenu();
                hasMenu = true;
            }
        }
        public override void SetWorkToIdle()
        {
            this.moveToBuild = false;
            this.isRepair = false;
            this.isCollectResource = false;
            this.reactionNeeded = false;
            this.checkMove = true;
            this.Busy = false;
        }

        public void SetWorkToBuild(string buildingType,Vector2 buildingPos)
        {
            this.typeOfBuildingWanted = buildingType;
            this.buildPosition = buildingPos;
            this.moveToBuild = true;
            this.Busy = true;
        }
    }
}
