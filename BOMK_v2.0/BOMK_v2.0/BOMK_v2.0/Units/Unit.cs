﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BOMK_v2._0
{
    public class Unit : BasicGameObjectWithHp
    {
        public bool automaticAttack = true;
        protected Vector2 unitDestination;
        protected Vector2 difference;
        public bool checkMove = true;
        protected MouseState oldMouseState;
        protected float timeSinceLastShot = 2f;
        protected float attackSpeed;
        protected float range;
        public bool Busy;
        public bool inSqaud;

        public Unit(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game, connectedPlayer)
        {

        }

        public Vector2 GetUnitDestination()
        {
            return unitDestination;
        }
        public void SetUnitDestination(Vector2 unitDestination)
        {
            this.unitDestination = unitDestination;
        }
        public MouseState GetOldMouseState()
        {
            return oldMouseState;
        }
        public void SetOldMouseState(MouseState mouseState)
        {
            this.oldMouseState = mouseState;
        }

        /// <summary>
        /// Calls the collision function that check for collision with other objects.
        /// Then moves towards unitDestination. 
        /// Only makes the move while checkMove=True
        /// When its location is this.unitDestination, it check CheckMove as False.
        /// </summary>
        public void Move()
        {
            if (this.unitDestination != new Vector2(0, 0))
            {
                difference = -this.GetMiddleVector() + unitDestination;
                Vector2 newpos = difference;
                newpos.Normalize();
                if (difference.Length() > 2 && this.checkMove)
                {
                    this.realPos += newpos;
                    this.Busy = true;
                }
                else
                {
                    this.checkMove = false;
                    this.Busy = false;
                }
            }
        }

        public virtual void UnitBehaviour()
        {

        }
        /// <summary>
        /// Checks if this unit's texture intersects with any other object's texture.
        /// If it does, it "pushes" him back by a unit vector away from the object.
        /// </summary>
        public void Collision()
        {
            Rectangle unitArea = this.GetDisplayRectangle();
            foreach (BasicGameObject o in this.game.GetObjectList())
            {
                if ((o is BasicGameObject) && this != o && !(o is Projectile) && !(o is RepairProjectile) && !(o is ImaginaryBuilding) && !(o is BasicButton))
                {
                    if (unitArea.Intersects(o.GetDisplayRectangle()))
                    {
                        Vector2 distance = -o.GetMiddleVector() + this.GetMiddleVector();
                        distance.Normalize();
                        this.realPos += distance;
                    }
                }
            }
        }
        public virtual void SetWorkToIdle()
        {

        }
    }
}
