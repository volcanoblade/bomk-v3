﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace BOMK_v2._0
{
    public class CombatUnit : Unit
    {
        public BasicGameObjectWithHp unitTarget;
        public bool isAttack = false;
        protected int buildCost;
        protected float attackSpeed;
        protected int range;
        public bool holding = false;
        protected bool waitingToHold = false;
        protected bool shouldHold = false;
        protected BasicGameObject holdedObject;
        public CombatUnit(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game, connectedPlayer)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public override void UnitBehaviour()
        {
            bool attackWanted = false;
            MouseState mouseState = Mouse.GetState();
            Point mousePosition = new Point(mouseState.X, mouseState.Y);
            if (this.isFocused && mouseState.RightButton == ButtonState.Pressed && this.connectedPlayer.isHumanPlayer)
            {  // making sure the computer is not effected by the human clicks.
                foreach (BasicGameObject o in game.GetObjectList())
                {
                    if (o.GetDisplayRectangle().Contains(mousePosition) && o is BasicGameObjectWithHp && this != o && !o.connectedPlayer.isHumanPlayer) // wants to attack
                    {
                        this.SetAttack(o as BasicGameObjectWithHp);
                        attackWanted = true;
                        this.automaticAttack = false;

                    }
                }
                if (!attackWanted)
                {
                    this.unitDestination.X = mouseState.X + this.game.cameraPos.X;
                    this.unitDestination.Y = mouseState.Y + this.game.cameraPos.Y;
                    this.checkMove = true;
                    this.isAttack = false;
                    this.automaticAttack = false;
                    this.Busy = true;
                }
            }
            this.HoldingManagment();
            this.Collision();
            this.Attack(this.unitTarget);
            this.Move();
            if (!this.Busy)
                this.automaticAttack = true;
            this.AutomaticAttackPlayer();
        }


        /// <summary>
        /// Gets a tatrget as BaseGameObjectWithHp.
        /// Check if this unit is not its own target, if the target is defined and if this unit is supposed to attack.
        /// Then, Checks if the target is within this units attack range.
        /// If it does, it decreases 1 from the traget's hp value.
        /// If it does not, it goes toward it.
        /// </summary>
        /// <param name="target"></param>
        /// 
        public void AutomaticAttackPlayer()
        {
            if (isAttack)
                return;
            if (this.automaticAttack && this.connectedPlayer.isHumanPlayer)
            {
                foreach (BasicGameObject o1 in this.game.computer.playerObjects.ToList())
                {
                   
                    BasicGameObjectWithHp objectWithHp = o1 as BasicGameObjectWithHp;
                    if (objectWithHp == null)
                        continue;
                    float distance = (-this.GetMiddleVector() + objectWithHp.GetMiddleVector()).Length();
                    if (distance < 300)
                    {
                        this.SetAttack(objectWithHp);
                    }
                }
            }

        }
        public virtual void Attack(BasicGameObjectWithHp target)
        {
            if (this != target && target != null && target.GetHp() > 0 && isAttack && target.connectedPlayer.isHumanPlayer != this.connectedPlayer.isHumanPlayer)
            {
                timeSinceLastShot += this.attackSpeed;
                Vector2 distance = target.GetMiddleVector() - this.GetMiddleVector();
                if (this.GetCircleRadius().Length() + this.range + target.GetCircleRadius().Length() >= distance.Length()) // if in range?
                {
                    if (timeSinceLastShot >= 2f)  // and its been enough time since the last shot?
                    {
                        Projectile shot = new Projectile(this.game.Content.Load<Texture2D>("Shot"), this.GetMiddleVector(), this.game, target, this.connectedPlayer, this);
                        this.game.AddToObjectList(shot); // fire!
                        timeSinceLastShot = 0f;
                        this.Busy = true;
                    }
                }
                else
                {
                    if (!holding) // if not in hold mode?
                    {
                        distance.Normalize();
                        this.realPos += distance;   // move in direction to the target.
                        this.Busy = true;
                    }
                }
            }
            else if ((target == null || target.GetHp() <= 0) && isAttack) // if should not be attacking?
            {
                this.isAttack = false;  // get out of attack mode and return to being not busy.
                this.Busy = false;
            }
        }

        public void SetAttack(BasicGameObjectWithHp target)
        {
            this.unitTarget = target;
            this.isAttack = true;
            this.checkMove = false;
            this.Busy = true;
            this.holding = false;
        }

        public void SetCombatToIdle()
        {
            this.isAttack = false;
            this.unitTarget = null;
            this.checkMove = true;
            Busy = false;
            this.holding = false;
            this.holdedObject = null;
            this.waitingToHold = false;
        }

        public void SetToDefend(BasicGameObject obj)
        {

        }
        /// <summary>
        /// Sets the unit to hold in its place and only attack if an enemy gets near.
        /// </summary>
        public void SetToHoldGround()
        {
            this.checkMove = false;
            this.holding = true;
        }
        public void SetToHoldGroundNearObject(BasicGameObject objToHold)
        {
            this.holdedObject = objToHold;
            this.waitingToHold = true;
            this.checkMove = false;
            this.Busy = true;
            this.holding = true;
        }
        /// <summary>
        /// !!
        /// </summary>
        public void HoldingManagment()
        {
            bool firstCheck = true;
            Vector2 minimalVector = new Vector2(0, 0);
            float minimalDistance = 0;
            Vector2 CompareVector = new Vector2(0, 0);
            BasicGameObjectWithHp targetObj = null;
            if (waitingToHold)
            {
                Vector2 distance = this.holdedObject.GetMiddleVector() - this.GetMiddleVector();
                if (distance.Length() <= 100f)
                {
                    this.SetToHoldGround();
                    this.waitingToHold = false;
                }
                else
                {
                    distance.Normalize();
                    this.realPos += distance;   // move in direction to the target.
                }
            }
            if (holding)
            {
                foreach (BasicGameObject obj in this.game.humanPlayer.playerObjects.ToList())
                {
                    if (obj is BasicGameObjectWithHp && obj != this)
                        CompareVector = -this.GetMiddleVector() + obj.GetMiddleVector();
                    if (firstCheck)
                    {
                        minimalDistance = CompareVector.Length();
                        targetObj = obj as BasicGameObjectWithHp;
                        firstCheck = false;
                    }
                    else
                    {
                        if (minimalDistance < CompareVector.Length())
                            continue;
                        else
                        {
                            minimalDistance = CompareVector.Length();
                            targetObj = obj as BasicGameObjectWithHp;
                        }
                    }
                }
                this.unitTarget = targetObj;
                this.isAttack = true;
            }

        }
    }


}

