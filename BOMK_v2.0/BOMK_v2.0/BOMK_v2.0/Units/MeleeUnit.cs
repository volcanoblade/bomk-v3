﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace BOMK_v2._0
{
    public class MeleeUnit : CombatUnit
    {

        public MeleeUnit(Vector2 drawPos, Texture2D texture, Game1 game, Player connectedPlayer)
            : base(drawPos, texture, game, connectedPlayer)
        {
            this.damage = 8;
            this.type = "MeleeUnit";
            this.buildCost = this.game.meleeCost;
            this.attackSpeed = 0.05f;
            this.range = 5;
            this.SetHp(100);
            this.connectedPlayer.playerResource = this.connectedPlayer.playerResource - this.buildCost;
            this.connectedPlayer.buildingUsage++;
        }
    }
}
