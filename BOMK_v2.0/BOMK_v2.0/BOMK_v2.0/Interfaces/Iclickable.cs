﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOMK_v2._0
{
    public interface IClickable
    {
        void DetectClick();
    }
}
