using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace BOMK_v2._0
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public Rectangle multiSelectRec;
        private MouseState previousMouseState;
        List<BasicGameObject> objectList; // List of ALL the objects in the game.
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        BasicGameObject focusedObject; // The focused object in this iteration presented by a BaseGameObject
        SpriteFont Font1;
        KeyboardState oldState;
        public Vector2 cameraPos;
        public int screenWidth;
        public int screenHeight;
        int mapWidth;
        int mapHeight;
        MainMenu mainMenu;
        public Player humanPlayer;
        public Player computer;
        public bool gameStarted = false;
        public bool gameEnded = false;
        public string losingPlayer;
        public Vector2 movingDirection;
        public int barracksCost = 15;
        public int baseCost = 50;
        public int towerCost = 25;
        public int houseCost = 25;
        public int meleeCost = 10;
        public int rangedCost = 15;
        public int workerCost = 5;
        protected bool errorDisplay;
        protected int ErrorDisplayTime;
        protected string ErrorText;
        Song inGameBackGroundMusic;
        Song MainMenuBackGroundMusic;
        Song losingMusic;
        Song winningMusic;
        private int timePassedSinceLastEnter = 0;
        public List<BasicGameObject> focusedObjectList;
        public float gameTimePassed = 0f;
        /// <summary>
        /// !!
        /// </summary>
        /// <param name="errorText"></param>
        public void EnableError(string errorText)
        {
            this.errorDisplay = true;
            this.ErrorDisplayTime = 200;
            this.ErrorText = errorText;
        }

        /// <summary>
        /// Returns the focused object in the game as a BaseGameObject
        /// </summary>
        /// <returns></returns>
        public BasicGameObject GetFocusedObject()
        {
            return this.focusedObject;
        }

        public bool IsObjectIsFocused(BasicGameObject obj)
        {
            if (this.focusedObject == null || this.focusedObjectList == null)
                return true;
            if ((obj == this.focusedObject) || (this.focusedObjectList.Contains(obj)))
                return true;
            else
                return false;
        }
        /// <summary>
        /// Gets a BaseGameObject and sets it as the focused object
        /// </summary>
        /// <param name="focusedObject"></param>
        public void SetFocusedObject(BasicGameObject focusedObject)
        {
            this.focusedObject = focusedObject;
        }

        /// <summary>
        /// returns the game's object list
        /// </summary>
        /// <returns></returns>
        public List<BasicGameObject> GetObjectList()
        {
            return this.objectList;
        }

        public bool IsExistInGameObjectList(BasicGameObject obj)
        {
            if (this.objectList.Contains(obj))
                return true;
            else
                return false;

        }

        /// <summary>
        /// Takes a BaseGameObject and adds it to the game's object list
        /// </summary>
        /// <param name="obj"></param>
        public void AddToObjectList(BasicGameObject obj)
        {
            this.objectList.Add(obj);
        }

        /// <summary>
        /// Gets an object as BaseGameObject and reomves it from the game's object list
        /// </summary>
        /// <param name="obj"></param>
        public void RemoveFromObjectList(BasicGameObject obj)
        {
            this.objectList.Remove(obj);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Game1()
        {
            mapWidth = 3000;
            mapHeight = 1600;
            screenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            screenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.graphics.PreferredBackBufferWidth = screenWidth;
            this.graphics.PreferredBackBufferHeight = screenHeight;

            this.graphics.IsFullScreen = false;
        }

        public void MultiSelect()
        {
            MouseState currentState = Mouse.GetState();
            if (currentState.LeftButton == ButtonState.Pressed)
            {
                if (previousMouseState.LeftButton == ButtonState.Released)
                {
                    multiSelectRec.X = currentState.X;
                    multiSelectRec.Y = currentState.Y;
                }
            }
            else
            {
                if (previousMouseState.LeftButton == ButtonState.Pressed)
                {
                    multiSelectRec.Width = currentState.X - multiSelectRec.X;
                    multiSelectRec.Height = currentState.Y - multiSelectRec.Y;
                    foreach (BasicGameObject o in this.objectList)
                    {
                        if (o is Unit && o.connectedPlayer.isHumanPlayer)
                        {
                            Unit selectedUnit = o as Unit;
                            if (multiSelectRec.Intersects(selectedUnit.GetDisplayRectangle()))
                                selectedUnit.SetIsFocused(true);
                        }
                    }
                }
            }
            previousMouseState = currentState;
        }
        public void MapGenerator()
        {
            Random rnd = new Random();
            for (int i = 0; i < 50; i++)
            {
                Vector2 checkVector = new Vector2(rnd.Next(0, mapWidth), rnd.Next(0, mapHeight));
                Resource resource = new Resource(checkVector, this.Content.Load<Texture2D>("Resource"), this, null);
                this.AddToObjectList(resource);

                foreach (BasicGameObject o in this.GetObjectList().ToList())
                {
                    if (o is Resource)
                    {
                        Resource r = o as Resource;

                        foreach (BasicGameObject o1 in this.GetObjectList().ToList())
                        {
                            if (r.GetDisplayRectangle().Intersects(o1.GetDisplayRectangle()) && (r != o1))
                            {
                                this.RemoveFromObjectList(r);
                            }
                        }
                    }
                }
            }
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            multiSelectRec = new Rectangle();
            objectList = new List<BasicGameObject>();
            humanPlayer = new Player(this, "Human"); // creates two players, for the human and for the AI.
            computer = new Player(this, "Computer");
            computer.AIManagerCreateFunction(humanPlayer, this); // takes the computerPlayer, HumanPlayer and the game.
            IsMouseVisible = true;
            this.gameStarted = false;
            mainMenu = new MainMenu(this);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            Font1 = Content.Load<SpriteFont>("Times New Roman");
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            //this.inGameBackGroundMusic = this.Content.Load<Song>("");
            this.MainMenuBackGroundMusic = this.Content.Load<Song>("MainMenuMusic");
            this.losingMusic = this.Content.Load<Song>("LosingMusic");
            this.winningMusic = this.Content.Load<Song>("WinningMusic");
            this.inGameBackGroundMusic = this.Content.Load<Song>("InGameMusic1");


            Vector2 d2 = new Vector2(300, 300);
            this.MapGenerator();
            // TODO: use this.Content to load your game content here
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// As long as the media player is playing do nothing. if it doesnt - play.
        /// </summary>
        public void Music()
        {
            if (!gameStarted)
            {
                if (MediaPlayer.State != MediaState.Playing)
                    MediaPlayer.Play(MainMenuBackGroundMusic);
            }
            else
            {
                if (gameEnded)
                {
                    if (losingPlayer == "Human")
                    {
                        if (MediaPlayer.State != MediaState.Playing)
                            MediaPlayer.Play(losingMusic);
                    }
                    else if (losingPlayer == "Computer")
                    {
                        if (MediaPlayer.State != MediaState.Playing)
                            MediaPlayer.Play(winningMusic);
                    }
                }
                else
                {
                    if (MediaPlayer.State != MediaState.Playing)
                        MediaPlayer.Play(inGameBackGroundMusic);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Update(GameTime gameTime)
        {

            this.Music();   // Controls the backgrounnd music.
            KeyboardState newState = Keyboard.GetState();
            if (newState.IsKeyDown(Keys.Escape))  // This is to make sure that pressing escape ALLWAYS exits the game.
                this.Exit();
            if (gameStarted)     // All action and functions that happen after the main menu.
            {
                this.humanPlayer.CheckIfLost();  // check if one of the players didnt lose yet
                this.computer.CheckIfLost();
                if (!gameEnded)  // if no one did.. then game
                {
                    this.InGame(gameTime);
                }
                else
                {

                }
            }
            else              // Functions while in menu here
            {
                timePassedSinceLastEnter++;
                if (newState.IsKeyDown(Keys.Enter) && this.mainMenu.typeOfFocusedButton == "Start")
                {
                    this.gameStarted = true;
                    MediaPlayer.Pause();
                }
                else if (newState.IsKeyDown(Keys.Enter) && this.mainMenu.typeOfFocusedButton == "Help")
                {
                    if (this.timePassedSinceLastEnter > 10)
                    {
                        this.timePassedSinceLastEnter = 0;

                        if (mainMenu.inHelpMenu)
                        {
                            this.mainMenu.inHelpMenu = false;
                        }
                        else
                        {
                            this.mainMenu.inHelpMenu = true;
                        }
                    }
                }
                else if (newState.IsKeyDown(Keys.Enter) && this.mainMenu.typeOfFocusedButton == "Quit")
                {
                    this.Exit();
                }
                this.mainMenu.MainMenuBehaviour();
            }
            this.MultiSelect();
        }

        private Vector2 MoveCamera()
        {
            KeyboardState newState = Keyboard.GetState();
            if (newState.IsKeyDown(Keys.Left))
            {
                if (cameraPos.X > 0)
                {
                    cameraPos.X -= 10;
                    return (new Vector2(-10, 0));
                }
                else
                {
                    return new Vector2(0, 0);
                }
            }
            else if (newState.IsKeyDown(Keys.Right))
            {
                if (cameraPos.X < mapWidth - screenWidth)
                {
                    cameraPos.X += 10;
                    return (new Vector2(10, 0));
                }
                else
                {
                    return new Vector2(0, 0);
                }
            }
            if (newState.IsKeyDown(Keys.Up))
            {
                if (cameraPos.Y > 0)
                {
                    cameraPos.Y -= 10;
                    return (new Vector2(0, -10));
                }
                else
                {
                    return new Vector2(0, 0);
                }
            }
            else if (newState.IsKeyDown(Keys.Down))
            {
                if (cameraPos.Y < mapHeight - screenHeight)
                {
                    cameraPos.Y += 10;
                    return (new Vector2(0, 10));
                }
                else
                {
                    return new Vector2(0, 0);
                }
            }           
            else
            {
                return new Vector2(0, 0);
            }

        }
        /// <summary>
        /// All functions and actions that should take place in-game.
        /// </summary>
        /// <param name="gameTime"></param>
        public void InGame(GameTime gameTime)
        {
            this.GameTimePassed();
            this.movingDirection = MoveCamera(); // controls the view movment.
            KeyboardState newState = Keyboard.GetState();
            oldState = newState;
            bool flag = false;
            foreach (BasicGameObject o in objectList.ToList())
            {
                o.loadObjectProperties();
                o.DetectClick();
                o.objectBehaviour();

                if (o.GetIsFocused())
                {
                    focusedObject = o;
                    flag = true;
                }
            }
            computer.computerAI.AIbehaviour();
            if (flag == false) // If each and each one is not focused then no one is focused
            {
                focusedObject = null;
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            KeyboardState newState = Keyboard.GetState();
            GraphicsDevice.Clear(Color.White);
            if (this.gameStarted) // After the main menu
            {
                if (!gameEnded)
                {
                    this.InGameDraw(gameTime);  // in game
                }
                else
                {
                    this.WinningInfoDraw(gameTime);  // After game stats
                }
            }
            else // main menu and pre-game
            {
                spriteBatch.Begin();
                if (mainMenu.inHelpMenu)
                {
                    this.DrawHelp();
                }
                this.mainMenu.Draw();
                spriteBatch.End();
            }
        }
        /// <summary>
        /// !!
        /// </summary>
        public void drawAfterGameStats()
        {
            this.spriteBatch.DrawString(Font1, "Stats:", new Vector2(this.screenWidth / 3, this.screenHeight / 4), Color.Black);
            this.spriteBatch.DrawString(Font1, "The game lasted :" + (int)this.gameTimePassed/60 + " minutes and " + this.gameTimePassed%60 + " seconds", new Vector2(this.screenWidth / 3, this.screenHeight / 4 + 30), Color.Black);
            this.spriteBatch.DrawString(Font1, "You inflicted: " + this.humanPlayer.damageInflictied + " damage", new Vector2(this.screenWidth / 3, this.screenHeight / 4 + 60), Color.Black);
            this.spriteBatch.DrawString(Font1, "The enemy inflicted: " + this.computer.damageInflictied + " damage", new Vector2(this.screenWidth / 3, this.screenHeight / 4 + 90), Color.Black);
        }
        /// <summary>
        /// !!
        /// </summary>
        /// <returns></returns>
        public string WinningInfo()
        {
            string winningInfoString;
            if (this.losingPlayer == "Computer")
                winningInfoString = "You won! well done!";
            else
                winningInfoString = "You lost... Maybe you should practice more or try an easier difficulty";

            return winningInfoString;
        }

        /// <summary>
        /// Draws the string that explain what is the game about and how to play while in mainmenu.
        /// </summary>
        public void DrawHelp()
        {
            this.spriteBatch.DrawString(Font1, "Welcome to BOMK: 'Battle Of Man Kind' An amateur 4x-rts-strategy game.", new Vector2(this.screenWidth / 5, this.screenHeight / 5), Color.Black);
            this.spriteBatch.DrawString(Font1, "You play as the blue units and fight the red units to victory.", new Vector2(this.screenWidth / 6, this.screenHeight / 4), Color.Black);
            this.spriteBatch.DrawString(Font1, "YOUR GOAL:", new Vector2(this.screenWidth / 6, this.screenHeight / 4 + 50), Color.Black);
            this.spriteBatch.DrawString(Font1, "Destroy all the enemy bases before the enemy destroys all of yours.", new Vector2(this.screenWidth / 6, this.screenHeight / 4 + 70), Color.Black);
            this.spriteBatch.DrawString(Font1, "CONTROLS:", new Vector2(this.screenWidth / 6, this.screenHeight / 4 + 120), Color.Black);
            this.spriteBatch.DrawString(Font1, "To scroll through the main menu you use the up & down buttons. You focus units with left-click.", new Vector2(this.screenWidth / 6, this.screenHeight / 4 + 140), Color.Black);
            this.spriteBatch.DrawString(Font1, "To interact with an object while focused on a unit you right-click the wanted object.", new Vector2(this.screenWidth / 6, this.screenHeight / 4 + 160), Color.Black);
            this.spriteBatch.DrawString(Font1, "Return to main menu: ENTER", new Vector2(this.screenWidth / 6, this.screenHeight - 100), Color.Black);
            this.spriteBatch.DrawString(Font1, "Exit the game: Esc", new Vector2(this.screenWidth / 6, this.screenHeight - 50), Color.Black);
        }

        public void DrawMenuGuide()
        {
            this.spriteBatch.DrawString(Font1, "Scroll buttons: Up & Down keys", new Vector2(this.screenWidth / 6, this.screenHeight - 150), Color.Black);
            this.spriteBatch.DrawString(Font1, "Choose: ENTER", new Vector2(this.screenWidth / 6, this.screenHeight - 100), Color.Black);
            this.spriteBatch.DrawString(Font1, "Exit the game: Esc", new Vector2(this.screenWidth / 6, this.screenHeight - 50), Color.Black);
        }

        public void GameTimePassed()
        {
            this.gameTimePassed += (float)this.TargetElapsedTime.TotalSeconds;
        }

        public void InGameDraw(GameTime gameTime)
        {
            spriteBatch.Begin();
            if (focusedObject != null)
            {
                if (focusedObject is BasicGameObjectWithHp)
                {
                    BasicGameObjectWithHp objWithHp = focusedObject as BasicGameObjectWithHp;
                    if (objWithHp.GetHp() > 0)
                    {
                        Vector2 hpPos = new Vector2(this.screenWidth - 750, this.screenHeight - 100);
                        Vector2 typePos = new Vector2(this.screenWidth - 750, this.screenHeight - 130);
                        Vector2 dmgPos = new Vector2(this.screenWidth - 750, this.screenHeight - 70);
                        Rectangle textureRectangle = new Rectangle(this.screenWidth - 850, this.screenHeight - 100, 50, 50);
                        string hp = "Health:  " + objWithHp.GetHp().ToString();
                        string type = objWithHp.GetObjectType();
                        string damage = "Damage:  " + objWithHp.GetDamage().ToString();
                        string texture = objWithHp.GetTexture();
                        if (texture != null && texture != "")
                        {
                            Texture2D drawTexture = this.Content.Load<Texture2D>(texture);
                            spriteBatch.DrawString(Font1, hp, hpPos, Color.Red); // Focused object's hp draw
                            spriteBatch.DrawString(Font1, type, typePos, Color.Black); //Foused object's type
                            spriteBatch.DrawString(Font1, damage, dmgPos, Color.Black); // Focused objects damage
                            spriteBatch.Draw(drawTexture, textureRectangle, Color.White); // Focused objects texture
                        }
                    }

                    if (focusedObject is WorkUnit)
                    {
                        WorkUnit workUnit = focusedObject as WorkUnit;
                        if (!workUnit.finished)
                        {
                            spriteBatch.DrawString(Font1, "Building... " + workUnit.timeToBuild.ToString() + "% left", new Vector2(this.screenWidth - 750, this.screenHeight - 160), Color.Black);
                        }

                    }
                }
                else if (focusedObject is Resource)
                {
                    Resource resource = focusedObject as Resource;
                    Vector2 fontPos = new Vector2(this.screenWidth - 1000, this.screenHeight - 100);
                    string hp = "This factory has: " + resource.supply.ToString() + " resources left";
                    spriteBatch.DrawString(Font1, hp, fontPos, Color.Black); // Focused object's hp draw
                }

            }
            spriteBatch.DrawString(Font1, "You own: " + this.humanPlayer.playerResource.ToString() + " resources", new Vector2(this.screenWidth - 300, this.screenHeight - 100), Color.Black);
            if (this.humanPlayer.IsPlayerBuildingUsageBelowCap())
                spriteBatch.DrawString(Font1, "Maintance: " + this.humanPlayer.buildingUsage + "/" + this.humanPlayer.buildingCapacity, new Vector2(this.screenWidth - 300, this.screenHeight - 80), Color.Black);
            else
                spriteBatch.DrawString(Font1, "Maintance: " + this.humanPlayer.buildingUsage + "/" + this.humanPlayer.buildingCapacity, new Vector2(this.screenWidth - 300, this.screenHeight - 80), Color.Red);
            Vector2 zeroVector = new Vector2(0, 0);
            foreach (IDrawablenBetter d in objectList.ToList())
            {
                d.Draw(spriteBatch);
            }
            if (this.errorDisplay)   // !!
            {
                if (this.ErrorDisplayTime > 0)
                {
                    spriteBatch.DrawString(Font1, this.ErrorText, new Vector2(100, 0), Color.Black);
                    this.ErrorDisplayTime--;
                }
                else
                {
                    this.errorDisplay = false;
                    this.ErrorDisplayTime = 200;
                }
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public void WinningInfoDraw(GameTime gameTime)
        {
            spriteBatch.Begin();
            string winningInfo = this.WinningInfo();
            spriteBatch.DrawString(Font1, winningInfo, new Vector2(100, 100), Color.Black);
            this.drawAfterGameStats();
            spriteBatch.End();
        }
    }
}
